#ifndef PQACCOUNT_H
#define PQACCOUNT_H

#include "pqobject.h"
#include "pqstatus.h"

struct _PurpleAccountUiOps;
struct _PurpleAccount;
struct _PurpleConversation;
class PQBuddy;
class PQConversation;
class PQNotification;

class PQAccount : public PQObject<_PurpleAccount>
{
Q_OBJECT
public:
  enum ConnectionState {
    Disconnected,
    Connecting,
    Connected
  };

  enum ConnectionError {
    NoError = -1,
    NetworkError,
    InvalidUsername,
    AuthenticationFailed,
    AuthenticationImpossible,
    NoSslSupport,
    EncryptionError,
    NameInUse,
    InvalidSettings,
    CertNotProvided,
    CertUntrusted,
    CertExpired,
    CertNotActivated,
    CertHostnameMismatch,
    CertFingerprintMismatch,
    CertSelfSigned,
    CertOtherError,
    OtherError
  };

  static PQAccount* create(const QString& username, const QString& protocol);
  static PQAccount* find(const QString& username, const QString& protocol);
  static QList<PQAccount*> findByUsername(const QString& username);
  static QList<PQAccount*> findByProtocolID(const QString& protocol);
  static QList<PQAccount*> allAccounts(bool update = false);
  ~PQAccount();

  // TODO: get/set UI strings, userinfo
  // TODO: public aliases, password changes, display names
  QString protocolID() const;
  QString protocolName() const;
  void setProtocolID(const QString& protocol);


  QString username() const;
  void setUsername(const QString& username);

  QString password() const;
  bool passwordSaved() const;
  void setPassword(const QString& username, bool save = true);

  QString alias() const;
  void setAlias(const QString& username);

  bool isEnabled() const;
  void setEnabled(bool enable);

  void registerAccount();
  void unregisterAccount();
  void deleteAccount(); // self-destructs object

  void* requestAuthorize(const QString& remoteUser, const QString& id, const QString& alias, const QString& message, bool onList, void(*acceptCB)(void*), void(*denyCB)(void*), void* cbData);
  void cancelRequest(void* handle);

  PQBuddy* addBuddy(const QString& name, const QString& alias = QString(), const QString& inviteMessage = QString());
  QList<PQBuddy*> buddies() const;

  ConnectionState connectionState() const;
  ConnectionError lastConnectionError() const;
  QString lastConnectionErrorMessage() const;
  static bool errorIsFatal(ConnectionError error);
  static void disconnectAll();

  QList<PQConversation*> openConversations() const;

public slots:
  void connect();
  void disconnect();

Q_SIGNALS:
  void connectionProgress(const QString& text, int step, int stepCount);
  void connectionError(PQAccount::ConnectionError errorType, const QString& error);
  void connected();
  void signedIn();
  void disconnected(PQAccount::ConnectionError errorType, const QString& error);
  // void connectionNotice(const QString& text); // unused by libpurple

  void registered(bool success);
  void unregistered(bool success);
  void notifyAdded(const QString& remoteUser, const QString& id, const QString& alias, const QString& message);
  void statusChanged(PQStatus status);
  void requestAdd(const QString& remoteUser, const QString& id, const QString& alias, const QString& message);

  // Note: Don't use a queued connection for this signal or you'll miss messages. You must catch this signal
  // and immediately connect to the conversation's signals.
  void newConversation(PQConversation* conversation);

  // Note: Don't use a queued connection for this signal or you'll miss messages. You must catch this signal
  // and immediately connect to the conversation's signals.
  void newBuddy(PQBuddy* buddy);

  void notification(PQNotification* notification);
  void closeNotification(PQNotification* notification);

private:
  friend class PQConversation;
  PQAccount(_PurpleAccount* account);
  QHash<_PurpleConversation*, PQConversation*> conversations;
};

#endif
