#ifndef PQOBJECT_H
#define PQOBJECT_H

#include <QObject>
#include <type_traits>
#include <utility>

template<
  class PQ,
  decltype(std::declval<typename PQ::BaseType*>()->node)* = nullptr
  >
PQ* pqobject_cast(typename PQ::BaseType* p)
{
  return reinterpret_cast<PQ*>(p->node.ui_data);
}

template<
  class PQ,
  decltype(std::declval<typename PQ::BaseType*>()->ui_data) = nullptr
  >
PQ* pqobject_cast(typename PQ::BaseType* p)
{
  return reinterpret_cast<PQ*>(p->ui_data);
}

template<
  class PType,
  decltype(std::declval<PType*>()->node)* = nullptr
  >
void PQObject_setUiData(PType* d, void* data)
{
  d->node.ui_data = data;
}

template<
  class PType,
  decltype(std::declval<PType*>()->ui_data) = nullptr
  >
void PQObject_setUiData(PType* d, void* data)
{
  d->ui_data = data;
}

template<typename Base>
void PQObject_noop_destructor(Base*) {}

template<typename Base, void (*destroy)(Base*) = PQObject_noop_destructor>
class PQObject : public QObject
{
public:
  using BaseType = Base;

  virtual ~PQObject() {
    destroy(d);
  }

  Base* purpleType() const
  {
    return d;
  }

  operator Base*() const
  {
    return purpleType();
  }

protected:
  PQObject(Base* d, QObject* parent) : QObject(parent), d(d) {
    PQObject_setUiData<BaseType>(d, this);
  }
  Base* d;
};

#define POBJECT_DESTRUCTOR(cname, destructor) \
  struct cname; extern "C" void destructor(cname*)

#ifndef Q_MOC_RUN
#  undef Q_SIGNALS
#  define Q_SIGNALS public
#endif

typedef void(*pqcallbackptr)();

template<typename... Args>
pqcallbackptr pqcallback(void(*callback)(Args...))
{
  union {
    void(*fptr)(Args...);
    pqcallbackptr vptr;
  } cast;
  cast.fptr = callback;
  return cast.vptr;
}

#define PQ_CALLBACK(fn, ...) \
  static void CB_ ## fn(__VA_ARGS__); \
  static pqcallbackptr fn = pqcallback(CB_ ## fn); \
  static void CB_ ## fn(__VA_ARGS__)

#endif
