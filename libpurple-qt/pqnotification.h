#ifndef PQNOTIFICATION_H
#define PQNOTIFICATION_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QHash>
#include <QVariant>
#include <functional>

struct _PurpleRequestFields;
struct _PurpleRequestField;

class PQNotification : public QObject
{
Q_OBJECT
public:
  /** @internal */
  static void initUiOps();

  ~PQNotification();

  enum NotificationType {
    Error,
    Warning,
    Info,
    Request,
    Uri,
  };

  struct Field {
    enum Type {
      None,
      String,
      Integer,
      Boolean,
      Choice,
      List,     // TODO: NYI
      Label,
      Image,    // TODO: NYI
      Account,  // TODO: NYI
    } type;

    QString id;
    QString label;
    QString typeHint;
    bool visible;
    bool required;
  };

  struct StringField : public Field {
    StringField() { type = String; }
    QString value;
    bool isMultiline;
    bool isMasked;
  };

  struct IntegerField : public Field {
    IntegerField() { type = Integer; }
    int value;
  };

  struct BooleanField : public Field {
    BooleanField() { type = Boolean; }
    bool value;
  };

  struct ChoiceField : public Field {
    ChoiceField() { type = Field::Choice; }
    struct Choice {
      QString label;
      int value;
    };
    QList<Choice> choices;
    int value;
  };

  // TODO: field groups NYI

  NotificationType type;
  QString title;
  QString primary;
  QString secondary;
  QString formattedText;
  QString uri;
  // TODO: Representation that doesn't require QtGui
  // QImage icon;

  QList<Field*> fields;
  QMap<QString, QVariant> fieldValues() const;
  QVariant fieldValue(int index) const;
  QVariant fieldValue(const QString& id) const;
  void setFieldValue(int index, const QVariant& value);
  void setFieldValue(const QString& id, const QVariant& value);

  QString okLabel;
  QString cancelLabel;

public slots:
  void ok();
  void cancel();

Q_SIGNALS:
  void closed(PQNotification* notification);

private:
  friend class PQNotificationPrivate;
  PQNotification();

  std::function<void()> okCallback;
  std::function<void()> cancelCallback;
  bool finished;

  _PurpleRequestFields* purpleFields;
  QHash<Field*, _PurpleRequestField*> fieldData;
  void close();
};

#endif
