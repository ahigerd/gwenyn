#include "pqnotification.h"
#include "pqcore.h"
#include "pqconversation.h"
#include "pqaccount.h"
#include "pqbuddy.h"
#include <QMetaObject>
#include <libpurple/notify.h>
#include <libpurple/request.h>
#include <QtDebug>
#include <cstdarg>
/*
#include <string>
#include <iostream>
*/

struct PQNotificationPrivate
{
  static PQNotification* create(const char* title, const char* primary, const char* secondary,
      const char* ok = nullptr, const char* cancel = nullptr)
  {
    PQNotification* notification = new PQNotification();
    notification->title = QString::fromUtf8(title);
    notification->primary = QString::fromUtf8(primary);
    notification->secondary = QString::fromUtf8(secondary);
    notification->okLabel = QString::fromUtf8(ok);
    notification->cancelLabel = QString::fromUtf8(cancel);
    return notification;
  }

  static void emitNotification(QObject* obj, PQNotification* notification)
  {
    notification->setParent(obj);
    QMetaObject::invokeMethod(obj, "notification", Q_ARG(PQNotification*, notification));
    QObject::connect(notification, SIGNAL(cancelled(PQNotification*)), obj, SLOT(cancelNotification(PQNotification*)));
  }

  static void emitNotification(PQNotification* notification, PurpleAccount* account, const char* who, PurpleConversation* conv)
  {
    if(conv) {
      emitNotification(pqobject_cast<PQConversation>(conv), notification);
    } else if(account) {
      PQAccount* pqaccount = pqobject_cast<PQAccount>(account);
      bool foundBuddy = false;
      if(who) {
        for(PQBuddy* buddy : pqaccount->buddies()) {
          if(buddy->name() == who) {
            foundBuddy = true;
            emitNotification(buddy, notification);
            break;
          }
        }
      }
      if(!foundBuddy) {
        emitNotification(pqaccount, notification);
      }
    } else {
      emitNotification(PQCore::instance(), notification);
    }
    qDebug() << notification->title << notification->primary << notification->secondary << notification->formattedText << notification->okLabel << notification->cancelLabel;
  }

  static void* notifyMessage(PurpleNotifyMsgType type, const char* title, const char* primary, const char* secondary)
  {
    qDebug() << "notify_message" << (int)type << title << primary << secondary;
    PQNotification* notification = create(title, primary, secondary);
    notification->type = (PQNotification::NotificationType)type;
    return nullptr;
  }

  static void* notifyFormatted(const char* title, const char* primary, const char* secondary, const char* text)
  {
    qDebug() << "notify_formatted" << title << primary << secondary << text;
    PQNotification* notification = create(title, primary, secondary);
    notification->type = PQNotification::Info;
    notification->formattedText = QString::fromUtf8(text);
    return notification;
  }

  static void* notifyUri(const char* uri)
  {
    qDebug() << "notify_uri" << uri;
    PQNotification* notification = new PQNotification();
    notification->type = PQNotification::Uri;
    notification->uri = QString::fromUtf8(uri);
    return notification;
  }

  static void closeNotify(PurpleNotifyType type, void* uiHandle)
  {
    Q_UNUSED(type);
    reinterpret_cast<PQNotification*>(uiHandle)->close();
  }

  static void* requestInput(const char* title, const char* primary, const char* secondary, const char* defaultValue,
     gboolean multiline, gboolean masked, gchar* hint, const char* okText, GCallback okCb, const char* cancelText,
     GCallback cancelCb, PurpleAccount* account, const char* who, PurpleConversation* conv, void* userData)
  {
    Q_UNUSED(multiline);
    Q_UNUSED(masked);
    PQNotification* notification = create(title, primary, secondary, okText, cancelText);

    PQNotification::StringField* field = new PQNotification::StringField();
    field->typeHint = QString::fromUtf8(hint);
    field->value = QString::fromUtf8(defaultValue);
    notification->fields << field;

    PurpleRequestInputCb realOkCb = reinterpret_cast<PurpleRequestInputCb>(okCb);
    PurpleRequestInputCb realCancelCb = reinterpret_cast<PurpleRequestInputCb>(cancelCb);

    notification->okCallback = [=]() {
      realOkCb(userData, field->value.toUtf8().constData());
    };
    notification->cancelCallback = [=]() {
      realCancelCb(userData, nullptr);
    };
    emitNotification(notification, account, who, conv);
    /*
    std::string response;
    std::getline(std::cin, response);
    field->value = QString::fromStdString(response);
    notification->ok();
    */
    return notification;
  }

  static void* requestChoice(const char* title, const char* primary, const char* secondary, int defaultValue,
      const char* okText, GCallback okCb, const char* cancelText, GCallback cancelCb, PurpleAccount* account,
      const char* who, PurpleConversation* conv, void* userData, va_list choices)
  {
    PQNotification* notification = create(title, primary, secondary, okText, cancelText);

    PQNotification::ChoiceField* field = new PQNotification::ChoiceField();
    while(true) {
      const char* label = va_arg(choices, const char*);
      if(!label) {
        break;
      }
      int value = va_arg(choices, int);
      field->choices << (PQNotification::ChoiceField::Choice){ QString::fromUtf8(label), value };
    }
    field->value = defaultValue;
    notification->fields << field;

    PurpleRequestChoiceCb realOkCb = reinterpret_cast<PurpleRequestChoiceCb>(okCb);
    PurpleRequestChoiceCb realCancelCb = reinterpret_cast<PurpleRequestChoiceCb>(cancelCb);

    notification->okCallback = [=]() {
      realOkCb(userData, field->value);
    };
    notification->cancelCallback = [=]() {
      realCancelCb(userData, defaultValue);
    };
    emitNotification(notification, account, who, conv);
    return notification;
  }

  static void* requestAction(const char* title, const char* primary, const char* secondary, int defaultAction,
      PurpleAccount* account, const char* who, PurpleConversation* conv, void* userData, size_t actionCount,
      va_list actions)
  {
    Q_UNUSED(title);
    Q_UNUSED(primary);
    Q_UNUSED(secondary);
    Q_UNUSED(defaultAction);
    Q_UNUSED(account);
    Q_UNUSED(who);
    Q_UNUSED(conv);
    Q_UNUSED(userData);
    Q_UNUSED(actionCount);
    Q_UNUSED(actions);
    // TODO: NYI
    return nullptr;
  }

  static void* requestFields(const char* title, const char* primary, const char* secondary, PurpleRequestFields* fields,
      const char* okText, GCallback okCb, const char* cancelText, GCallback cancelCb, PurpleAccount* account,
      const char* who, PurpleConversation* conv, void* userData)
  {
    Q_UNUSED(title);
    Q_UNUSED(primary);
    Q_UNUSED(secondary);
    Q_UNUSED(fields);
    Q_UNUSED(okText);
    Q_UNUSED(okCb);
    Q_UNUSED(cancelText);
    Q_UNUSED(cancelCb);
    Q_UNUSED(account);
    Q_UNUSED(who);
    Q_UNUSED(conv);
    Q_UNUSED(userData);
    // TODO: NYI
    return nullptr;
  }

  static void closeRequest(PurpleRequestType type, void* uiHandle)
  {
    Q_UNUSED(type);
    reinterpret_cast<PQNotification*>(uiHandle)->close();
  }

  static void* requestActionWithIcon(const char* title, const char* primary, const char* secondary, int defaultAction,
      PurpleAccount* account, const char* who, PurpleConversation* conv, gconstpointer iconData, gsize iconSize,
      void* userData, size_t actionCount, va_list actions)
  {
    Q_UNUSED(iconData);
    Q_UNUSED(iconSize);
    return requestAction(title, primary, secondary, defaultAction, account, who, conv, userData, actionCount, actions);
  }
};

static PurpleNotifyUiOps PQNotify_uiOps = {
  &PQNotificationPrivate::notifyMessage, // notify_message
  nullptr, // notify_email
  nullptr, // notify_emails
  &PQNotificationPrivate::notifyFormatted, // notify_formatted
  nullptr, // notify_searchresults
  nullptr, // notify_searchresults_new_rows
  nullptr, // notify_userinfo
  &PQNotificationPrivate::notifyUri, // notify_uri
  nullptr, // close_notify
  nullptr, // reserved
  nullptr, // reserved
  nullptr, // reserved
  nullptr, // reserved
};

static PurpleRequestUiOps PQRequest_uiOps = {
  &PQNotificationPrivate::requestInput, // request_input
  &PQNotificationPrivate::requestChoice, // request_choice
  &PQNotificationPrivate::requestAction, // request_action
  &PQNotificationPrivate::requestFields, // request_fields
  nullptr, // request_file
  &PQNotificationPrivate::closeRequest, // close_request
  nullptr, // request_folder
  &PQNotificationPrivate::requestActionWithIcon, // request_action_with_icon
  nullptr, // reserved
  nullptr, // reserved
  nullptr, // reserved
};

void PQNotification::initUiOps()
{
  purple_notify_set_ui_ops(&PQNotify_uiOps);
  purple_request_set_ui_ops(&PQRequest_uiOps);
}

PQNotification::PQNotification()
: type(Request), finished(false)
{
  // initializers only
}

PQNotification::~PQNotification()
{
  if(!finished) {
    if(!cancelLabel.isEmpty() && cancelCallback) {
      cancel();
    } else {
      if(fields.length() > 0) {
        // Notifications without fields don't demand a response, so there's no reason to warn about that.
        qWarning("PQNotification::~PQNotification: destroying an unfinished, uncancelable request");
      }
      ok();
    }
  }
  for(Field* field : fields) {
    delete field;
  }
}

void PQNotification::ok()
{
  if(!finished) {
    finished = true;
    if(!okLabel.isEmpty() && okCallback) {
      okCallback();
    }
  }
}

void PQNotification::cancel()
{
  if(!finished) {
    finished = true;
    if(!cancelLabel.isEmpty() && cancelCallback) {
      cancelCallback();
    }
  }
}

QMap<QString, QVariant> PQNotification::fieldValues() const
{
  QMap<QString, QVariant> result;
  int len = fields.length();
  for(int i = 0; i < len; i++) {
    if(fields[i]->id.isEmpty()) {
      continue;
    }
    result[fields[i]->id] = fieldValue(i);
  }
  return result;
}

QVariant PQNotification::fieldValue(int index) const
{
  if(index < 0 || index > fields.length()) {
    return QVariant();
  }
  Field* field = fields[index];
  switch(field->type) {
    case Field::String:
      return static_cast<StringField*>(field)->value;
      break;
    case Field::Integer:
      return static_cast<IntegerField*>(field)->value;
      break;
    case Field::Boolean:
      return static_cast<BooleanField*>(field)->value;
      break;
    case Field::Choice:
      return static_cast<ChoiceField*>(field)->value;
      break;
    default:
      return QVariant();
  }
}

QVariant PQNotification::fieldValue(const QString& id) const
{
  int len = fields.length();
  for(int i = 0; i < len; i++) {
    if(fields[i]->id == id) {
      return fieldValue(i);
    }
  }
  return QVariant();
}

void PQNotification::setFieldValue(int index, const QVariant& value)
{
  if(index < 0 || index > fields.length()) {
    return;
  }
  Field* field = fields[index];
  switch(field->type) {
    case Field::String:
      static_cast<StringField*>(field)->value = value.toString();
      break;
    case Field::Integer:
      static_cast<IntegerField*>(field)->value = value.toInt();
      break;
    case Field::Boolean:
      static_cast<BooleanField*>(field)->value = value.toBool();;
      break;
    case Field::Choice:
      static_cast<ChoiceField*>(field)->value = value.toInt();
      break;
    default:
      return;
  }
}

void PQNotification::setFieldValue(const QString& id, const QVariant& value)
{
  int len = fields.length();
  for(int i = 0; i < len; i++) {
    if(fields[i]->id == id) {
      setFieldValue(i, value);
      return;
    }
  }
}

void PQNotification::close()
{
  finished = true;
  emit closed(this);
}
