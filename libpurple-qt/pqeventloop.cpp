#include "pqeventloop.h"

// This file is derived from nullclient.c from the libpurple example sources, modified
// to be valid C++ and modified to be consistent with Gwenyn's coding style.

#define PURPLE_GLIB_READ_COND  (G_IO_IN | G_IO_HUP | G_IO_ERR)
#define PURPLE_GLIB_WRITE_COND (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)

struct PQIOClosure {
  PurpleInputFunction function;
  guint result;
  gpointer data;
};

static void PQEventLoop_destroyClosure(gpointer data)
{
  delete reinterpret_cast<PQIOClosure*>(data);
}

static gboolean PQEventLoop_invokeClosure(GIOChannel *source, GIOCondition condition, gpointer data)
{
  PQIOClosure* closure = reinterpret_cast<PQIOClosure*>(data);
  int purple_cond = 0;

  if(condition & PURPLE_GLIB_READ_COND)
    purple_cond |= PURPLE_INPUT_READ;
  if(condition & PURPLE_GLIB_WRITE_COND)
    purple_cond |= PURPLE_INPUT_WRITE;

  closure->function(closure->data, g_io_channel_unix_get_fd(source), (PurpleInputCondition)purple_cond);

  return true;
}

static guint PQEventLoop_inputAdd(gint fd, PurpleInputCondition condition, PurpleInputFunction function, gpointer data)
{
  PQIOClosure *closure = new PQIOClosure;
  int cond = 0;

  closure->function = function;
  closure->data = data;

  if(condition & PURPLE_INPUT_READ)
    cond |= PURPLE_GLIB_READ_COND;
  if(condition & PURPLE_INPUT_WRITE)
    cond |= PURPLE_GLIB_WRITE_COND;

  GIOChannel* channel = g_io_channel_unix_new(fd);
  closure->result = g_io_add_watch_full(channel, G_PRIORITY_DEFAULT, (GIOCondition)cond,
      PQEventLoop_invokeClosure, closure, PQEventLoop_destroyClosure);

  g_io_channel_unref(channel);
  return closure->result;
}

PurpleEventLoopUiOps PQEventLoop_uiOps =
{
  g_timeout_add,
  g_source_remove,
  PQEventLoop_inputAdd,
  g_source_remove,
  nullptr,
  g_timeout_add_seconds,
  nullptr,
  nullptr,
  nullptr
};
