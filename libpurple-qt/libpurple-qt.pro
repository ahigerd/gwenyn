TEMPLATE     = lib
TARGET       = purple-qt
INCLUDEPATH += .
DESTDIR      = ../
OBJECTS_DIR  = .tmp
MOC_DIR      = .tmp
CONFIG      += staticlib link_pkgconfig create_prl debug
CONFIG      -= release debug_and_release
QT           = core network
PKGCONFIG    = purple glib-2.0
QMAKE_CXXFLAGS += -O0 -ggdb

HEADERS += pqobject.h

HEADERS += pqcore.h   pqaccount.h   pqstatus.h   pqeventloop.h   pqconversation.h   pqbuddy.h   pqnotification.h
SOURCES += pqcore.cpp pqaccount.cpp pqstatus.cpp pqeventloop.cpp pqconversation.cpp pqbuddy.cpp pqnotification.cpp
