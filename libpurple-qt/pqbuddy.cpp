#include "pqbuddy.h"
#include "pqaccount.h"
#include <libpurple/blist.h>

QList<PQBuddy*> PQBuddy::allBuddies()
{
  QList<PQBuddy*> result;
  GSList* buddies = purple_blist_get_buddies();
  GSList* node = buddies;
  while(node) {
    PurpleBuddy* buddy = reinterpret_cast<PurpleBuddy*>(node->data);
    if(!buddy->node.ui_data) {
      PQAccount* account = pqobject_cast<PQAccount>(purple_buddy_get_account(buddy));
      if(!account) {
        // Needs to be initialized
        PQAccount::allAccounts();
        account = pqobject_cast<PQAccount>(purple_buddy_get_account(buddy));
      }
      buddy->node.ui_data = new PQBuddy(buddy, account);
    }
    result << pqobject_cast<PQBuddy>(buddy);
    node = g_slist_next(node);
  }
  return result;
}

PQBuddy::PQBuddy(_PurpleBuddy* buddy, QObject* parent)
: PQObject(buddy, parent)
{
  // initializers only
}

PQAccount* PQBuddy::account() const
{
  return pqobject_cast<PQAccount>(purple_buddy_get_account(d));
}

QString PQBuddy::name() const
{
  return QString::fromUtf8(purple_buddy_get_name(d));
}

QString PQBuddy::alias() const
{
  return QString::fromUtf8(purple_buddy_get_alias(d));
}

QString PQBuddy::statusName() const
{
  PurplePresence* ppresence = purple_buddy_get_presence(d);
  PurpleStatus* pstatus = purple_presence_get_active_status(ppresence);
  return QString::fromUtf8(purple_status_get_name(pstatus));
}

