#ifndef PQCORE_H
#define PQCORE_H

#include "pqobject.h"
#include <QObject>
#include <QStringList>
#include <QDir>

class PQCorePrivate;
class PQNotification;

class PQCore : public QObject
{
Q_OBJECT
public:
  PQCore(const QString& ui, QObject* parent = 0);
  ~PQCore();

  static PQCore* instance();

  QString uiName() const;
  bool ensureSingleInstance();

  static void requestShutdown();

  static QString userDir();
  static void setUserDir(const QString& path);

  bool hasPreference(const QString& name) const;
  QVariant preference(const QString& name) const;
  QStringList preferenceChildren(const QString& parent) const;
  void setPreference(const QString& name, bool value);
  void setPreference(const QString& name, int value);
  void setPreference(const QString& name, const QString& value);
  void setPreference(const QString& name, const QStringList& value);
  void setPreference(const QString& name, const QDir& value);
  void setPreference(const QString& name, const QList<QDir>& value);
  void removePreference(const QString& name);

public slots:
  void start();

Q_SIGNALS:
  void initialized();
  void shutdown();
  void networkIsUp();
  void networkIsDown();
  void notification(PQNotification* notification);
  void closeNotification(PQNotification* notification);

private slots:
  void aboutToShutdown();

private:
  bool coreIsShutdown;
  QString ui;
};
Q_DECLARE_METATYPE(QDir);
Q_DECLARE_METATYPE(QList<QDir>);

#endif
