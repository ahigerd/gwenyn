#ifndef PQBUDDY_H
#define PQBUDDY_H

#include "pqobject.h"

struct _PurpleBuddy;
class PQAccount;
class PQNotification;

class PQBuddy : public PQObject<_PurpleBuddy>
{
Q_OBJECT
public:
  /** @internal */
  PQBuddy(_PurpleBuddy* buddy, QObject* parent);

  static QList<PQBuddy*> allBuddies();

  PQAccount* account() const;
  QString name() const;
  QString alias() const;
  QString statusName() const;

Q_SIGNALS:
  void notification(PQNotification* notification);
  void closeNotification(PQNotification* notification);
};

#endif
