#include "pqaccount.h"
#include "pqstatus.h"
#include "pqcore.h"
#include "pqconversation.h"
#include "pqbuddy.h"
#include <libpurple/account.h>
#include <libpurple/connection.h>
#include <libpurple/blist.h>
#include <libpurple/debug.h>
#include <QMap>
#include <QtDebug>

static QMap<qintptr, PQAccount*> authRequestOwners;
static qintptr nextAuthHandle = 0;

PQAccount* getAccountForPurple(PurpleAccount* paccount)
{
  if(!paccount->ui_data) {
    PQAccount::allAccounts(true);
  }
  return pqobject_cast<PQAccount>(paccount);
}

static PurpleAccountUiOps accountUiOps = {
  [](PurpleAccount* acct, const char* user, const char* id, const char* alias, const char* msg) {
    getAccountForPurple(acct)->notifyAdded(user, id, alias, msg);
  },
  [](PurpleAccount* acct, PurpleStatus* status) {
    getAccountForPurple(acct)->statusChanged(status);
  },
  [](PurpleAccount* acct, const char* user, const char* id, const char* alias, const char* msg) {
    getAccountForPurple(acct)->requestAdd(user, id, alias, msg);
  },
  [](PurpleAccount* acct, const char* user, const char* id, const char* alias, const char* msg, gboolean onList, void(*acceptCB)(void*), void(*denyCB)(void*), void* cbData) -> void* {
    return getAccountForPurple(acct)->requestAuthorize(user, id, alias, msg, onList, acceptCB, denyCB, cbData);
  },
  [](void* handle) {
    authRequestOwners[reinterpret_cast<qintptr>(handle)]->cancelRequest(handle);
  },
  nullptr,
  nullptr,
  nullptr,
  nullptr
};

static PurpleConnectionUiOps connectionUiOps = {
  [] (PurpleConnection* c, const char* text, size_t step, size_t count) {
    getAccountForPurple(purple_connection_get_account(c))->connectionProgress(text, step, count);
  },
  [] (PurpleConnection* c) {
    getAccountForPurple(purple_connection_get_account(c))->connected();
  },
  [] (PurpleConnection* c) {
    getAccountForPurple(purple_connection_get_account(c))->disconnected(PQAccount::NoError, "");
  },
  nullptr, // PQAccount_notice,
  [] (PurpleConnection* c, const char* text) {
    getAccountForPurple(purple_connection_get_account(c))->disconnected(PQAccount::OtherError, QString::fromUtf8(text));
  },
  [] { PQCore::instance()->networkIsUp(); },
  [] { PQCore::instance()->networkIsDown(); },
  [] (PurpleConnection* c, PurpleConnectionError reason, const char* text) {
    getAccountForPurple(purple_connection_get_account(c))->disconnected(PQAccount::ConnectionError(reason), QString::fromUtf8(text));
  },
  nullptr,
  nullptr,
  nullptr,
};

static bool uiOpsInitialized = false;

PQ_CALLBACK(PQAccount_signedIn, PurpleAccount* acct)
{
  getAccountForPurple(acct)->signedIn();
}

PQ_CALLBACK(PQAccount_connectionError, PurpleAccount* acct, PurpleConnectionError reason, const gchar* message)
{
  getAccountForPurple(acct)->connectionError(PQAccount::ConnectionError(reason), QString::fromUtf8(message));
}

PQ_CALLBACK(PQAccount_receivedIM, PurpleAccount* acct, char* sender, char* message, PurpleConversation* conv, PurpleMessageFlags flags)
{
  PQAccount* pacct = getAccountForPurple(acct);
  PQConversation* pconv = nullptr;
  if(conv) {
    pconv = pqobject_cast<PQConversation>(conv);
    if(!pconv) {
      pconv = new PQConversation(conv);
      emit pacct->newConversation(pconv);
    }
  } else {
    pconv = new PQConversation(pacct, sender, PQConversation::Direct);
    emit pacct->newConversation(pconv);
  }
  pconv->receivedMessage(QString::fromUtf8(sender), QString::fromUtf8(message), (PQConversation::MessageFlags)flags);
}

PQ_CALLBACK(PQAccount_newBuddyNode, PurpleBlistNode* node)
{
  auto nodeType = purple_blist_node_get_type(node);
  if(nodeType == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy* buddy = PURPLE_BUDDY(node);
    PQBuddy* pbuddy = pqobject_cast<PQBuddy>(buddy);
    if(!pbuddy) {
      pbuddy = new PQBuddy(buddy, getAccountForPurple(purple_buddy_get_account(buddy)));
    }
    emit pbuddy->account()->newBuddy(pbuddy);
  } else if(nodeType == PURPLE_BLIST_CONTACT_NODE) {
    qDebug() << "TODO: unhandled contact node";
  } else if(nodeType == PURPLE_BLIST_GROUP_NODE) {
    qDebug() << "TODO: unhandled group node";
  } else if(nodeType == PURPLE_BLIST_CHAT_NODE) {
    qDebug() << "TODO: unhandled chat node";
  } else if(nodeType == PURPLE_BLIST_OTHER_NODE) {
    qDebug() << "TODO: unhandled other node";
  }
}

PQAccount::PQAccount(_PurpleAccount* account)
: PQObject(account, PQCore::instance())
{
  if(!uiOpsInitialized) {
    purple_set_blist(purple_blist_new());
    purple_blist_load();

    purple_accounts_set_ui_ops(&accountUiOps);
    purple_connections_set_ui_ops(&connectionUiOps);
    purple_signal_connect(purple_accounts_get_handle(), "account-signed-on", this, PQAccount_signedIn, nullptr);
    purple_signal_connect(purple_accounts_get_handle(), "account-connection-error", this, PQAccount_connectionError, nullptr);
    purple_signal_connect(purple_conversations_get_handle(), "received-im-msg", this, PQAccount_receivedIM, this);
    purple_signal_connect(purple_blist_get_handle(), "blist-node-added", this, PQAccount_newBuddyNode, this);
    uiOpsInitialized = true;
  }
}

static QList<PQAccount*> PQAccount_allAccounts;
QList<PQAccount*> PQAccount::allAccounts(bool update)
{
  if(update) {
    PQAccount_allAccounts.clear();
  }
  if(PQAccount_allAccounts.length() == 0) {
    GList* list = purple_accounts_get_all();
    while(list) {
      PurpleAccount* purpleAccount = reinterpret_cast<PurpleAccount*>(list->data);
      PQAccount* account = pqobject_cast<PQAccount>(purpleAccount);
      if(!account) {
        account = new PQAccount(purpleAccount);
        purple_debug_misc("libpurple-qt", "created PQAccount object from within allAccounts");
      }
      PQAccount_allAccounts << account;
      list = list->next;
    }
  }
  return PQAccount_allAccounts;
}

PQAccount* PQAccount::create(const QString& username, const QString& protocol)
{
  if(!protocol.startsWith("prpl-")) {
    purple_debug_warning("libpurple-qt", "PQAccount::create(): protocol '%s' does not begin with 'prpl-'", protocol.toUtf8().constData());
  }
  _PurpleAccount* account = purple_account_new(username.toUtf8().constData(), protocol.toUtf8().constData());
  purple_accounts_add(account);
  PQAccount_allAccounts.clear(); // force requerying
  return new PQAccount(account);
}

PQAccount* PQAccount::find(const QString& username, const QString& protocol)
{
  if(!protocol.startsWith("prpl-")) {
    purple_debug_warning("libpurple-qt", "PQAccount::create(): protocol '%s' does not begin with 'prpl-'", protocol.toUtf8().constData());
  }
  _PurpleAccount* account = purple_accounts_find(username.toUtf8().constData(), protocol.toUtf8().constData());
  return pqobject_cast<PQAccount>(account);
}

QList<PQAccount*> PQAccount::findByUsername(const QString& username)
{
  QList<PQAccount*> result;
  for(PQAccount* account : allAccounts()) {
    if(account->username() == username) {
      result << account;
    }
  }
  return result;
}

QList<PQAccount*> PQAccount::findByProtocolID(const QString& protocol)
{
  QList<PQAccount*> result;
  for(PQAccount* account : allAccounts()) {
    if(account->protocolID() == protocol) {
      result << account;
    }
  }
  return result;
}

PQAccount::~PQAccount()
{
}

void PQAccount::connect()
{
  purple_account_connect(d);
}

void PQAccount::disconnect()
{
  purple_account_disconnect(d);
}

void PQAccount::disconnectAll()
{
  purple_connections_disconnect_all();
}

PQAccount::ConnectionState PQAccount::connectionState() const
{
  if(purple_account_is_connecting(d)) {
    return Connecting;
  }
  return purple_account_is_connected(d) ? Connected : Disconnected;
}

PQAccount::ConnectionError PQAccount::lastConnectionError() const
{
  return (PQAccount::ConnectionError)purple_account_get_current_error(d)->type;
}

QString PQAccount::lastConnectionErrorMessage() const
{
  return QString::fromUtf8(purple_account_get_current_error(d)->description);
}

bool PQAccount::errorIsFatal(ConnectionError error)
{
  return purple_connection_error_is_fatal((PurpleConnectionError)error);
}

void PQAccount::registerAccount()
{
  purple_account_set_register_callback(d, [](PurpleAccount* acct, gboolean succeeded, void*) {
    getAccountForPurple(acct)->registered(succeeded);
  }, nullptr);
  purple_account_register(d);
}

void PQAccount::unregisterAccount()
{
  purple_account_unregister(d, [](PurpleAccount* acct, gboolean succeeded, void*) {
    getAccountForPurple(acct)->unregistered(succeeded);
  }, nullptr);
}

void PQAccount::deleteAccount()
{
  purple_accounts_delete(d);
  d = nullptr;
  deleteLater();
}

QString PQAccount::protocolID() const
{
  return QString::fromUtf8(purple_account_get_protocol_id(d));
}

QString PQAccount::protocolName() const
{
  return QString::fromUtf8(purple_account_get_protocol_name(d));
}

void PQAccount::setProtocolID(const QString& protocol)
{
  if(!protocol.startsWith("prpl-")) {
    purple_debug_warning("libpurple-qt", "PQAccount::create(): protocol '%s' does not begin with 'prpl-'", protocol.toUtf8().constData());
  }
  purple_account_set_protocol_id(d, protocol.toUtf8().constData());
}

QString PQAccount::username() const
{
  return QString::fromUtf8(purple_account_get_username(d));
}

void PQAccount::setUsername(const QString& username)
{
  purple_account_set_username(d, username.toUtf8().constData());
}

QString PQAccount::password() const
{
  return QString::fromUtf8(purple_account_get_password(d));
}

bool PQAccount::passwordSaved() const
{
  return purple_account_get_remember_password(d);
}

void PQAccount::setPassword(const QString& password, bool save)
{
  purple_account_set_remember_password(d, save);
  purple_account_set_password(d, password.toUtf8().constData());
}

QString PQAccount::alias() const
{
  return QString::fromUtf8(purple_account_get_alias(d));
}

void PQAccount::setAlias(const QString& alias)
{
  purple_account_set_alias(d, alias.toUtf8().constData());
}

bool PQAccount::isEnabled() const
{
  return purple_account_get_enabled(d, PQCore::instance()->uiName().toUtf8().constData());
}

void PQAccount::setEnabled(bool enable)
{
  purple_account_set_enabled(d, PQCore::instance()->uiName().toUtf8().constData(), enable);
}

void* PQAccount::requestAuthorize(const QString& remoteUser, const QString& id, const QString& alias, const QString& message, bool onList, void(*acceptCB)(void*), void(*denyCB)(void*), void* cbData)
{
  // TODO
  qintptr handle = nextAuthHandle;
  authRequestOwners[handle] = this;
  nextAuthHandle++;
  return reinterpret_cast<void*>(this);
}

void PQAccount::cancelRequest(void* handle)
{
  authRequestOwners.remove(reinterpret_cast<qintptr>(handle));
  // TODO
}

PQBuddy* PQAccount::addBuddy(const QString& name, const QString& alias, const QString& inviteMessage)
{
  PurpleBuddy* buddy = purple_buddy_new(d, name.toUtf8().constData(), alias.isEmpty() ? nullptr : alias.toUtf8().constData());
  purple_blist_add_buddy(buddy, nullptr, nullptr, nullptr);
  if(inviteMessage.isEmpty()) {
    purple_account_add_buddy(d, buddy);
  } else {
    purple_account_add_buddy_with_invite(d, buddy, inviteMessage.toUtf8().constData());
  }
  return new PQBuddy(buddy, this);
}

QList<PQBuddy*> PQAccount::buddies() const
{
  QList<PQBuddy*> result;
  for(PQBuddy* buddy : PQBuddy::allBuddies()) {
    if(buddy->account() == this) {
      result << buddy;
    }
  }
  return result;
}

QList<PQConversation*> PQAccount::openConversations() const
{
  // TODO: this isn't O(1), improve?
  return conversations.values();
}
