#ifndef PQCONVERSATION_H
#define PQCONVERSATION_H

#include "pqobject.h"
#include <QFlags>

class PQAccount;
class PQNotification;

POBJECT_DESTRUCTOR(_PurpleConversation, purple_conversation_destroy);
class PQConversation : public PQObject<_PurpleConversation, purple_conversation_destroy>
{
Q_OBJECT
public:
  enum Type {
    Unknown = 0,
    Direct,
    Group,
    Misc,
    Any
  };

  enum MessageFlag {
    Send            = 0x0001,
    Receive         = 0x0002,
    System          = 0x0004,
    AutoResponse    = 0x0008,
    ActiveOnly      = 0x0010,
    NickHighlight   = 0x0020,
    NoLog           = 0x0040,
    Whisper         = 0x0080,
    // 0x0100 appears to be unused
    Error           = 0x0200,
    Delayed         = 0x0400,
    NoFormatting    = 0x0800,
    ContainsImages  = 0x1000,
    Notify          = 0x2000,
    NoLinkify       = 0x4000,
    Invisible       = 0x8000
  };
  Q_DECLARE_FLAGS(MessageFlags, MessageFlag);

  enum BuddyFlag {
    None            = 0x0000,
    Voice           = 0x0001,
    HalfOp          = 0x0002,
    Operator        = 0x0004,
    Founder         = 0x0008,
    Typing          = 0x0010,
    Away            = 0x0020
  };
  Q_DECLARE_FLAGS(BuddyFlags, BuddyFlag);

  PQConversation(PQAccount* account, const QString& name, Type type);
  PQConversation(_PurpleConversation* conversation);
  ~PQConversation();

  PQAccount* account() const;
  QString name() const;
  Type type() const;

public slots:
  void sendMessage(const QString& message);
  void sendMessage(const QString& message, PQConversation::MessageFlags flags);

Q_SIGNALS:
  void receivedMessage(const QString& sender, const QString& message, PQConversation::MessageFlags flags);
  void notification(PQNotification* notification);
  void closeNotification(PQNotification* notification);
};
Q_DECLARE_OPERATORS_FOR_FLAGS(PQConversation::MessageFlags);
Q_DECLARE_OPERATORS_FOR_FLAGS(PQConversation::BuddyFlags);

#endif
