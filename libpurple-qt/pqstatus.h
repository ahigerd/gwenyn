#ifndef PQSTATUS_H
#define PQSTATUS_H

#include <libpurple/value.h>
#include <QVariant>

struct _PurpleStatus;

class PQStatus
{
public:
  PQStatus(_PurpleStatus* status);
  // Probably won't ever need a mutable status

  QString id() const;
  QString name() const;
  bool isIndependent() const;
  bool isExclusive() const;
  bool isAvailable() const;
  bool isActive() const;
  bool isOnline() const;

  QVariant attributeValue(const QString& id) const;

  template<typename T>
  T attribute(const QString& id) const
  {
    return attributeValue(id).value<T>();
  }

private:
  _PurpleStatus* status;
};

#endif
