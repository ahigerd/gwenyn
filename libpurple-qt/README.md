libpurple-qt
============

This is a C++/Qt binding for libpurple.

This library was built primarily for use by the server-side component of Gwenyn.
This means that features useful to Gwenyn are going to be the first to be
implemented, while other features may be slower to be added.

The binding uses the common subset of libpurple 2.x and libpurple 3.x APIs, so
it should in theory support being compiled against either version, but to date
it has only been tested against 2.x.

Pull requests to add support for missing features or to address 3.x-specific
issues are welcome.

Limitations
-----------

While Qt is natively cross-platform, libpurple requires the use of glib. This
means that using libpurple-qt on Windows and OS X requires rebuilding Qt to
use the glib event loop instead of using the native one. It is theoretically
possible to provide an alternate set of event loop bindings to libpurple, but
this has not yet been successful.

Due to libpurple-qt's use of some C++11 features, it currently requires the use
of Qt 5. Qt 4 support should be feasible without too much extra work, so it may
be added if there is demand for it. Pull requests for Qt 4 support are also
welcome.
