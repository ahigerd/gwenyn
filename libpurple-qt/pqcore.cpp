#include "pqcore.h"
#include "pqeventloop.h"
#include "pqnotification.h"
#include <glib.h>
#include <libpurple/debug.h>
#include <libpurple/core.h>
#include <libpurple/util.h>
#include <libpurple/prefs.h>
#include <libpurple/notify.h>
#include <libpurple/request.h>
#include <QHash>
#include <QtGlobal>
#include <QtDebug>

static PQCore* PQCore_instance = nullptr;

PQCore* PQCore::instance()
{
  return PQCore_instance;
}

static void PQCore_initialized()
{
  PQCore_instance->initialized();
}

static void PQCore_shutdown()
{
  if(PQCore_instance)
    PQCore_instance->shutdown();
}

static PurpleCoreUiOps PQCore_uiOps = {
  nullptr,
  nullptr,
  PQCore_initialized,
  PQCore_shutdown,
  nullptr,
  nullptr,
  nullptr,
  nullptr
};

static PurpleDebugUiOps PQDebug_uiOps = {
  [](PurpleDebugLevel level, const char* category, const char* message) {
    switch(level) {
      case PURPLE_DEBUG_ALL:
        qDebug() << QString("[all] %1: %2").arg(category).arg(message).trimmed().toUtf8().constData();
        break;
      case PURPLE_DEBUG_MISC:
        qDebug() << QString("[misc] %1: %2").arg(category).arg(message).trimmed().toUtf8().constData();
        break;
      case PURPLE_DEBUG_INFO:
        qDebug() << QString("[info] %1: %2").arg(category).arg(message).trimmed().toUtf8().constData();
        break;
      case PURPLE_DEBUG_WARNING:
        qWarning() << QString("[warn] %1: %2").arg(category).arg(message).trimmed().toUtf8().constData();
        break;
      case PURPLE_DEBUG_ERROR:
        qWarning() << QString("[error] %1: %2").arg(category).arg(message).trimmed().toUtf8().constData();
        break;
      case PURPLE_DEBUG_FATAL:
        qCritical() << QString("[fatal] %1: %2").arg(category).arg(message).trimmed().toUtf8().constData();
        break;
    }
  },
  [](PurpleDebugLevel level, const char*) -> gboolean {
    // TODO: get fancy
    return true;
  },
  nullptr,
  nullptr,
  nullptr,
  nullptr,
};

PQCore::PQCore(const QString& ui, QObject* parent)
: QObject(parent), coreIsShutdown(false), ui(ui)
{
  qRegisterMetaType<QDir>();
  qRegisterMetaType<QList<QDir>>();

  PQCore_instance = this;
  QObject::connect(this, SIGNAL(shutdown()), this, SLOT(aboutToShutdown()));

  purple_debug_set_ui_ops(&PQDebug_uiOps);
  purple_core_set_ui_ops(&PQCore_uiOps);
  purple_eventloop_set_ui_ops(&PQEventLoop_uiOps);
  PQNotification::initUiOps();
}

PQCore::~PQCore()
{
  if(!coreIsShutdown) {
    coreIsShutdown = true;
    purple_core_quit();
  }
  PQCore_instance = nullptr;
  // TODO: clean up all timers and notifiers
}

void PQCore::start()
{
  if(!purple_core_init(ui.toUtf8().constData())) {
    qFatal("Failed to initialize libpurple core!");
  }

  purple_prefs_load();
  //purple_plugins_load_saved(PLUGIN_SAVE_PREF);
}

bool PQCore::ensureSingleInstance()
{
  return purple_core_ensure_single_instance();
}

void PQCore::requestShutdown()
{
  purple_timeout_add(0, purple_core_quit_cb, nullptr);
}

void PQCore::aboutToShutdown()
{
  coreIsShutdown = true;
}

QString PQCore::uiName() const
{
  return ui;
}

QString PQCore::userDir()
{
  return QString::fromUtf8(purple_user_dir());
}

// This should be called before calling PQCore::start()
void PQCore::setUserDir(const QString& path)
{
  purple_util_set_user_dir(path.toUtf8().constData());
}

bool PQCore::hasPreference(const QString& name) const
{
  return purple_prefs_exists(name.toUtf8().constData());
}

QVariant PQCore::preference(const QString& name) const
{
  if(!hasPreference(name)) {
    return QVariant();
  }

  const char* cname = name.toUtf8().constData();
  int type = purple_prefs_get_type(cname);
  switch(type) {
  case PURPLE_PREF_BOOLEAN:
    return purple_prefs_get_bool(cname);
  case PURPLE_PREF_INT:
    return purple_prefs_get_int(cname);
  case PURPLE_PREF_STRING:
    return QString::fromUtf8(purple_prefs_get_string(cname));
  case PURPLE_PREF_STRING_LIST:
    {
      GList* glist = purple_prefs_get_string_list(cname);
      QStringList result;
      while(glist) {
        result << QString::fromUtf8((const char*)glist->data);
        glist = g_list_next(glist);
      }
      return result;
    }
  case PURPLE_PREF_PATH:
    return QVariant::fromValue(QDir(QString::fromUtf8(purple_prefs_get_string(cname))));
  case PURPLE_PREF_PATH_LIST:
    {
      GList* glist = purple_prefs_get_string_list(cname);
      QList<QDir> result;
      while(glist) {
        result << QDir(QString::fromUtf8((const char*)glist->data));
        glist = g_list_next(glist);
      }
      return QVariant::fromValue(result);
    }
  default:
    return QVariant();
  }
}

QStringList PQCore::preferenceChildren(const QString& parent) const
{
  GList* glist = purple_prefs_get_children_names(parent.toUtf8().constData());
  QStringList result;
  while(glist) {
    result << QString::fromUtf8((const char*)glist->data);
    glist = g_list_next(glist);
  }
  return result;
}

void PQCore::setPreference(const QString& name, bool value)
{
  purple_prefs_set_bool(name.toUtf8().constData(), value);
}

void PQCore::setPreference(const QString& name, int value)
{
  purple_prefs_set_int(name.toUtf8().constData(), value);
}

void PQCore::setPreference(const QString& name, const QString& value)
{
  purple_prefs_set_string(name.toUtf8().constData(), value.toUtf8().constData());
}

void PQCore::setPreference(const QString& name, const QStringList& value)
{
  GList* glist = nullptr;
  QList<QByteArray> byteArrays;
  for(int i = value.length() - 1; i >= 0; --i) {
    byteArrays.append(value[i].toUtf8());
    glist = g_list_prepend(glist, (gpointer)byteArrays.last().constData());
  }
  purple_prefs_set_string_list(name.toUtf8().constData(), glist);
}

void PQCore::setPreference(const QString& name, const QDir& value)
{
  purple_prefs_set_path(name.toUtf8().constData(), value.path().toUtf8().constData());
}

void PQCore::setPreference(const QString& name, const QList<QDir>& value)
{
  GList* glist = nullptr;
  QList<QByteArray> byteArrays;
  for(int i = value.length() - 1; i >= 0; --i) {
    byteArrays.append(value[i].path().toUtf8());
    glist = g_list_prepend(glist, (gpointer)byteArrays.last().constData());
  }
  purple_prefs_set_string_list(name.toUtf8().constData(), glist);
}

void PQCore::removePreference(const QString& name)
{
  purple_prefs_remove(name.toUtf8().constData());
}
