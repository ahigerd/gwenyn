#include "pqconversation.h"
#include "pqaccount.h"
#include <libpurple/conversation.h>
#include <QtDebug>

PQConversation::PQConversation(PQAccount* account, const QString& name, PQConversation::Type type)
: PQObject(purple_conversation_new((PurpleConversationType)type, account->purpleType(), name.toUtf8().constData()), account)
{
  // TODO: register in a list of conversation objects on the account
  account->conversations[d] = this;
}

PQConversation::PQConversation(_PurpleConversation* conversation)
: PQObject(conversation, pqobject_cast<PQAccount>(purple_conversation_get_account(conversation)))
{
  // TODO: register in a list of conversation objects on the account
  account()->conversations[d] = this;
}

PQConversation::~PQConversation()
{
  account()->conversations.remove(d);
}

PQAccount* PQConversation::account() const
{
  return pqobject_cast<PQAccount>(purple_conversation_get_account(d));
}

QString PQConversation::name() const
{
  return QString::fromUtf8(purple_conversation_get_name(d));
}

PQConversation::Type PQConversation::type() const
{
  return (PQConversation::Type)purple_conversation_get_type(d);
}

void PQConversation::sendMessage(const QString& message)
{
  if(type() == Direct) {
    purple_conv_im_send(purple_conversation_get_im_data(d), message.toUtf8().constData());
  } else {
    purple_conv_chat_send(purple_conversation_get_chat_data(d), message.toUtf8().constData());
  }
}

void PQConversation::sendMessage(const QString& message, PQConversation::MessageFlags flags)
{
  if(type() == Direct) {
    purple_conv_im_send_with_flags(purple_conversation_get_im_data(d), message.toUtf8().constData(), (PurpleMessageFlags)(int)flags);
  } else {
    purple_conv_chat_send_with_flags(purple_conversation_get_chat_data(d), message.toUtf8().constData(), (PurpleMessageFlags)(int)flags);
  }
}
