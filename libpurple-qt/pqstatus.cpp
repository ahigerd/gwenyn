#include "pqstatus.h"
#include <libpurple/status.h>

PQStatus::PQStatus(_PurpleStatus* status)
: status(status)
{
  // initializers only
}

QString PQStatus::id() const
{
  return purple_status_get_id(status);
}

QString PQStatus::name() const
{
  return purple_status_get_name(status);
}

bool PQStatus::isIndependent() const
{
  return purple_status_is_independent(status);
}

bool PQStatus::isExclusive() const
{
  return purple_status_is_exclusive(status);
}

bool PQStatus::isAvailable() const
{
  return purple_status_is_available(status);
}

bool PQStatus::isActive() const
{
  return purple_status_is_active(status);
}

bool PQStatus::isOnline() const
{
  return purple_status_is_online(status);
}

QVariant PQStatus::attributeValue(const QString& id) const
{
  // TODO
  return QVariant();
}
