TEMPLATE = subdirs
SUBDIRS = common client
unix {
  SUBDIRS += libpurple-qt server worker
}
server.depends = common worker
worker.depends = common libpurple-qt
client.depends = common
