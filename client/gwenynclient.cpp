#include "gwenynclient.h"
#include <QEventLoop>
#include <QSettings>
#include <QNetworkConfigurationManager>
#include "qxtmetaobject.h"
#include "qxtdatastreamsignalserializer.h"

static GwenynClient* clientSingleton = 0;

GwenynClient* GwenynClient::instance()
{
  return clientSingleton;
}

GwenynClient::GwenynClient(QObject* parent)
: QObject(parent), gwenynInstance(0), nextCookie(0)
{
  clientSingleton = this;

  m_network = new QNetworkConfigurationManager(this);
  QObject::connect(m_network, SIGNAL(configurationAdded(QNetworkConfiguration)), this, SLOT(sendPing()));
  QObject::connect(m_network, SIGNAL(configurationChanged(QNetworkConfiguration)), this, SLOT(sendPing()));
  QObject::connect(m_network, SIGNAL(configurationRemoved(QNetworkConfiguration)), this, SLOT(sendPing()));
  QObject::connect(m_network, SIGNAL(onlineStateChanged(bool)), this, SLOT(sendPing()));

  static_cast<QxtDataStreamSignalSerializer*>(m_peer.serializer())->setDataStreamVersion(12);
  m_peer.setDevice(&m_socket);
  gwenynInstance = new Gwenyn(&m_peer, false);

  using namespace std::placeholders;
  GwenynProto.partyChat.bind(std::bind(&GwenynClient::partyChat, this, _2, _3));
  m_peer.attachSlot("directMessage", this, SIGNAL(directMessage(Buddy, QString)), Qt::DirectConnection);
  m_peer.attachSlot("groupMessage", this, SIGNAL(groupMessage(QString, Buddy, QString)), Qt::DirectConnection);
  // TODO: passing serialized IRCNames is ugly, make it Buddy
  m_peer.attachSlot("buddyState", this, SIGNAL(buddyInfo(QString, QString, QString)), Qt::DirectConnection);
  QObject::connect(&m_socket, SIGNAL(connected()), this, SLOT(connected()));
  QObject::connect(&m_socket, SIGNAL(encrypted()), this, SLOT(encrypted()));
  QObject::connect(&m_socket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors(QList<QSslError>)));
  QObject::connect(&m_socket, SIGNAL(disconnected()), &m_reconnectTimer, SLOT(start()));
  QObject::connect(&m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(disconnected()));
  QObject::connect(&m_socket, SIGNAL(readyRead()), &m_pingTimer, SLOT(start()));
  QObject::connect(&m_socket, SIGNAL(readyRead()), &m_pongTimer, SLOT(stop()));

  m_reconnectTimer.setInterval(5000);
  m_reconnectTimer.setSingleShot(true);
  QObject::connect(&m_reconnectTimer, SIGNAL(timeout()), this, SLOT(reconnect()));

  m_pingTimer.setSingleShot(false);
  m_pingTimer.setInterval(300000);
  QObject::connect(&m_pingTimer, SIGNAL(timeout()), this, SLOT(sendPing()));

  m_pongTimer.setSingleShot(true);
  m_pongTimer.setInterval(3000); // TODO: is this reasonable? handle slower links? maybe adaptive ping?
  QObject::connect(&m_pongTimer, SIGNAL(timeout()), this, SLOT(pingTimeout()));
}

void GwenynClient::setUsername(const QString& name)
{
  m_username = name;
}

QString GwenynClient::username() const
{
  return m_username;
}

void GwenynClient::connect(const QString& host, int port)
{
  m_lastHost = host;
  m_lastPort = port;
  m_socket.connectToHostEncrypted(host, port);
}

void GwenynClient::connected()
{
  // no-op right now
  emit systemMessage("Starting connection...");
  m_sentDisconnectMessage = false;
}

void GwenynClient::encrypted()
{
  emit systemMessage("Connected to server");
  // TODO: pulling from settings is sort of an ugly hack, but storing the password in RAM isn't great either
  call("ident", m_username,
    QString::fromUtf8(QByteArray::fromBase64(QSettings().value("connection/password").toByteArray())));
  auto callInfo = GwenynProto.partyChat("bitlbee", "connect");
  callInfo->setTimeout(10000);
  m_lastPing = QDateTime::currentDateTime();
  m_pingQueued = false;
  emit connectedToServer();
}

void GwenynClient::sslErrors(const QList<QSslError>& errors)
{
  // TODO: host verification if desired
  for(const QSslError& err : errors) {
    emit systemMessage(err.errorString());
  }
  m_socket.ignoreSslErrors();
}

bool GwenynClient::waitForConnected()
{
  if(m_peer.isClient() && m_socket.mode() == QSslSocket::SslClientMode) return true;
  QEventLoop loop;
  QObject::connect(&m_socket, SIGNAL(encrypted()), &loop, SLOT(quit()));
  QObject::connect(&m_socket, SIGNAL(disconnected()), &loop, SLOT(quit()));
  QObject::connect(&m_socket, SIGNAL(error(QAbstractSocket::SocketError)), &loop, SLOT(quit()));
  loop.exec();
  return m_peer.isClient();
}

void GwenynClient::disconnected()
{
  Gwenyn::instance()->abortAllRPCs();
  m_pingTimer.stop();
  m_pongTimer.stop();
  if(!m_sentDisconnectMessage) {
    m_sentDisconnectMessage = true;
    emit systemMessage("Disconnected from server");
    emit disconnectedFromServer();
    reconnect();
  } else {
    m_reconnectTimer.start();
  }
}

void GwenynClient::reconnect()
{
  if(!m_network->isOnline()) {
    return;
  }
  qDebug() << "Reconnecting...";
  m_peer.takeDevice();
  m_socket.abort();
  m_peer.setDevice(&m_socket);
  connect(m_lastHost, m_lastPort);
}

void GwenynClient::sendPing()
{
  if(m_lastPing > QDateTime::currentDateTime().addSecs(-3)) {
    if(!m_pingQueued) {
      m_pingQueued = true;
      QTimer::singleShot(3000, this, SLOT(sendPing()));
    }
    return;
  }
  if(m_socket.state() == QAbstractSocket::ConnectedState) {
    m_pingQueued = false;
    m_lastPing = QDateTime::currentDateTime();
    qDebug() << "Sending ping";
    auto callInfo = GwenynProto.ping();
    callInfo->setTimeout(10000);
    callInfo->except([this](AbstractCallInfo*, int, const QVariantList&) {
      this->pingTimeout();
    });
  }
}

void GwenynClient::pingTimeout()
{
  qDebug() << "Ping timeout";
  // will fire auto-reconnect
  m_peer.takeDevice();
  m_socket.abort();
  m_peer.setDevice(&m_socket);
}

void GwenynClient::receivePartyChat(qulonglong cookie, const QString& sender, const QString& message)
{
  GwenynProto.partyChat.ack(cookie);
  emit partyChat(sender, message);
}

void GwenynClient::sendPartyChat(const QString& receiver, const QString& message)
{
  auto callInfo = GwenynProto.partyChat(receiver, message);
  callInfo->except(bind_partyChatError);
}

void GwenynClient::partyChatError(AbstractCallInfo* callInfo, int errorCode, const QVariantList& params)
{
  auto ci = callInfo->cast(GwenynProto.partyChat);
  logError(callInfo, errorCode, params);
  qDebug() << ci->functionName() << ci->argumentList();
  retry(callInfo);
}

void GwenynClient::logError(AbstractCallInfo* callInfo, int errorCode, const QVariantList& params) const
{
  qDebug() << Gwenyn::error(errorCode, params) << callInfo->functionName() << callInfo->argumentList();
  emit systemMessage(callInfo->functionName() + ": " + Gwenyn::error(errorCode, params));
}

void GwenynClient::retry(AbstractCallInfo* callInfo)
{
  Q_UNUSED(callInfo);
}
