#include "chatpane.h"
#include "gwenynclient.h"
#include <QDateTime>
#include <QTextBrowser>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QKeyEvent>
#include <QRegExp>

ChatPane::ChatPane(const QString& target, QWidget* parent)
: QWidget(parent), m_target(target), m_unread(0)
{
  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);

  m_log = new QTextBrowser(this);
  m_log->setOpenExternalLinks(true);
  m_log->setFocusPolicy(Qt::ClickFocus);
  layout->addWidget(m_log, 1);

  m_message = new QLineEdit(this);
  layout->addWidget(m_message, 0);

  QObject::connect(m_message, SIGNAL(returnPressed()), this, SLOT(sendMessage()));
  QObject::connect(m_message, SIGNAL(textChanged(QString)), this, SIGNAL(markRead()));
  m_log->installEventFilter(this);
  m_message->installEventFilter(this);
}

void ChatPane::showEvent(QShowEvent*)
{
  m_message->setFocus();
}

bool ChatPane::eventFilter(QObject* obj, QEvent* event)
{
  if(event->type() == QEvent::KeyPress) {
    QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
    if(keyEvent->key() == Qt::Key_Tab && keyEvent->modifiers() & (Qt::ControlModifier | Qt::MetaModifier)) {
      // TODO: Does this sense get inverted for RTL?
      if(keyEvent->modifiers() & Qt::ShiftModifier) {
        emit tabLeft();
      } else {
        emit tabRight();
      }
      return true;
    } else if(keyEvent->key() == Qt::Key_W && keyEvent->modifiers() & Qt::ControlModifier) {
      emit closeTab();
      return true;
    } else if(keyEvent->key() == Qt::Key_F4 && keyEvent->modifiers() & Qt::ControlModifier) {
      emit closeTab();
      return true;
    } else if(keyEvent->modifiers() & (Qt::MetaModifier | Qt::AltModifier | Qt::ControlModifier)) {
      if(keyEvent->key() == Qt::Key_Left || keyEvent->key() == Qt::Key_BraceLeft || keyEvent->key() == Qt::Key_BracketLeft) {
        emit tabLeft();
        return true;
      } else if(keyEvent->key() == Qt::Key_Right || keyEvent->key() == Qt::Key_BraceRight || keyEvent->key() == Qt::Key_BracketRight) {
        emit tabRight();
        return true;
      } else {
        return false;
      }
    } else if(keyEvent->key() == Qt::Key_Escape && keyEvent->modifiers() == Qt::NoModifier) {
      m_message->clear();
      return true;
    } else if(obj == m_log && !keyEvent->text().isEmpty()) {
      m_message->setFocus();
      m_message->end(false);
      m_message->event(event);
      return true;
    }
  }
  return false;
}

static const char invalidChars[] = {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 14, 15, 16, 17, 18,
  19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 127
};

void ChatPane::sendMessage()
{
  QString message = m_message->text();
  message.replace("\r\n", "\n");
  message.replace('\r', '\n');
  for(char ch : invalidChars) {
    message.remove(ch);
  }

  addMessage(GwenynClient::instance()->username(), message);
  emit sendMessage(m_target, message);
  m_message->setText("");
  m_message->setFocus();
}

static QRegExp twitterURL("(https://t.co/[^ >]+)>? &lt;([^> ]+)>");
static QRegExp twitterID("\x02\\[\x02([0-9a-f]+):([0-9a-f]+)(?:->([0-9a-f]+):([0-9a-f]+))?\x02\\]\x02");
static QRegExp defaultURL("(https?://[^ \t\n\r>]+)", Qt::CaseInsensitive);
static QRegExp ampersandURL("(<a href='[^']*&amp;[^']*'>)");
static QRegExp boldFormatting("\x02([^\x02]*)\x02");
static QRegExp italicFormatting("\x1D([^\x1D]*)\x1D");
static QRegExp underlineFormatting("\x1F([^\x1F]*)\x1F");

void ChatPane::addMessage(const QString& sender, const QString& message, bool unread)
{
  if(unread) {
    m_unread++;
  }

  QString prefix;
  QString richMessage = message;
  if(richMessage.startsWith("<HTML><BODY>") && richMessage.endsWith("</BODY></HTML>")) {
    // preformatted HTML coming from AIM, don't apply IRC formatting or URL autodetection
    richMessage
      .remove(0, sizeof("<HTML><BODY>") - 1)
      .chop(sizeof("</BODY></HTML>") - 1);
  } else {
    if(twitterID.indexIn(richMessage) >= 0) {
      QStringList caps = twitterID.capturedTexts();
      quint64 tweetID = caps[1].toLongLong(nullptr, 16);
      QString replacement;
      if (caps.length() == 5 && !caps[3].isEmpty()) {
        quint64 replyID = caps[3].toLongLong(nullptr, 16);
        prefix = QString("[<a href='https://twitter.com/%1/status/%2'>%3</a>-><a href='https://twitter.com/%4/status/%5'>%6</a>]")
          .arg(sender).arg(tweetID).arg(caps[2])
          .arg(sender).arg(replyID).arg(caps[4]); // TODO: embed reply-to username instead of fudging with sender
      } else {
        prefix = QString("[<a href='https://twitter.com/%1/status/%2'>%3</a>]")
          .arg(sender).arg(tweetID).arg(caps[2]);
      }
      richMessage.replace(caps[0], "");
    }

    richMessage
      .replace("&", "&amp;")
      .replace("<", "&lt;")
      .replace(boldFormatting, "<b>\\1</b>")
      .replace(italicFormatting, "<i>\\1</i>")
      .replace(underlineFormatting, "<u>\\1</u>");

    bool foundURL = false;
    if(richMessage.contains("https://t.co/") && twitterURL.indexIn(richMessage) >= 0) {
      // Twitter URL escaping
      richMessage.replace(twitterURL, "<a href='\\1'>\\2</a>");
      foundURL = true;
    } else if(defaultURL.indexIn(richMessage) >= 0) {
      // Auto-URL detection
      richMessage.replace(defaultURL, "<a href='\\1'>\\1</a>");
      foundURL = true;
    }
    if(foundURL) {
      int pos = 0;
      while(ampersandURL.indexIn(richMessage, pos) >= 0) {
        pos += ampersandURL.matchedLength() - 3;
        richMessage.replace(ampersandURL.cap(1), QString(ampersandURL.cap(1)).replace("&amp;", "&"));
      }
    }
  }

  for(char ch : invalidChars) {
    if(ch == 0)
      richMessage.replace(QChar(ch), QChar(0x24EA)); // circled 0
    else if(ch < 21)
      richMessage.replace(QChar(ch), QChar(0x2460 + (ch - 1))); // circled 1-20
    else if(ch < 32)
      richMessage.replace(QChar(ch), QChar(0x3250 + (ch - 20))); // circled 21-35
    else
      richMessage.replace(QChar(ch), QChar(0x2326)); // DEL
  }

  m_log->append(QString("<!doc>[%2] &lt;<b>%1</b>&gt; %3").arg(sender).arg(QDateTime::currentDateTime().toString("hh:mm AP")).arg(prefix + richMessage));
  m_log->verticalScrollBar()->triggerAction(QAbstractSlider::SliderToMaximum);
}

int ChatPane::unreadCount() const
{
  return m_unread;
}

void ChatPane::clearUnread()
{
  m_unread = 0;
}

void ChatPane::focusInput()
{
  m_message->setFocus();
}
