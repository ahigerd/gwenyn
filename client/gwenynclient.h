#ifndef GWENYNCLIENT_H
#define GWENYNCLIENT_H

#include "qxtrpcservice.h"
#include "gwenynproto.h"
#include <QSslSocket>
#include <QTimer>
#include <QDateTime>
class QNetworkConfigurationManager;

class GwenynClient : public QObject
{
Q_OBJECT
public:
  static GwenynClient* instance();

  GwenynClient(QObject* parent = 0);

  void setUsername(const QString& name);
  QString username() const;

  void connect(const QString& host, int port);
  bool waitForConnected();

  template <typename... Args>
  qulonglong call(const QString& fn, Args&&... args)
  {
    qulonglong cookie = ++nextCookie;
    QVariantList variants = packVariantList(args...);
    m_pongTimer.start();
    m_peer.call(fn, cookie, variants.value(0), variants.value(1), variants.value(2), variants.value(3), variants.value(4), variants.value(5), variants.value(6));
    return cookie;
  }

public slots:
  void sendPartyChat(const QString& receiver, const QString& message);
  void receivePartyChat(qulonglong cookie, const QString& sender, const QString& message);

signals:
  void connectedToServer();
  void disconnectedFromServer();
  void partyChat(const QString& sender, const QString& message);
  void directMessage(const Buddy& buddy, const QString& message);
  void groupMessage(const QString& room, const Buddy& buddy, const QString& message);
  void systemMessage(const QString& message) const;
  // TODO: serialized IRCName is bad
  void buddyInfo(const QString& buddy, const QString& status, const QString& statusMessage);

private slots:
  void encrypted();
  void connected();
  void sslErrors(const QList<QSslError>& errors);
  void disconnected();
  void reconnect();
  void sendPing();
  void pingTimeout();

private:
  void logError(AbstractCallInfo* callInfo, int errorCode, const QVariantList& params) const;
  void retry(AbstractCallInfo* callInfo);
  RPC_ERROR_CALLBACK(GwenynClient, partyChatError);

  Gwenyn* gwenynInstance;
  QNetworkConfigurationManager* m_network;
  QxtRPCService m_peer;
  QSslSocket m_socket;
  QString m_username;
  qulonglong nextCookie;
  QString m_lastHost;
  int m_lastPort;
  QTimer m_pingTimer;
  QTimer m_pongTimer;
  QTimer m_reconnectTimer;
  bool m_sentDisconnectMessage;
  QDateTime m_lastPing;
  bool m_pingQueued;
};

#endif
