#include "settingsdialog.h"
#include <QSettings>
#include <QFormLayout>
#include <QLineEdit>
#include <QIntValidator>
#include <QDialogButtonBox>
#include <QPushButton>

SettingsDialog::SettingsDialog(QWidget* parent)
: QDialog(parent)
{
  QSettings settings;
  setWindowTitle(tr("Connection Settings"));
  QFormLayout* layout = new QFormLayout(this);

  // TODO: persistence
  m_server = new QLineEdit(this);
  m_server->setText(settings.value("connection/server", "localhost").toString());
  layout->addRow(tr("&Server:"), m_server);

  m_port = new QLineEdit(this);
  m_port->setValidator(new QIntValidator(1, 65535, this));
  m_port->setText(settings.value("connection/port", 60668).toString());
  if(!m_port->text().toInt())
    m_port->setText("60668");
  layout->addRow(tr("&Port:"), m_port);

  m_username = new QLineEdit(this);
  m_username->setText(settings.value("connection/username").toString());
  layout->addRow(tr("&Username:"), m_username);

  m_password = new QLineEdit(this);
  m_password->setText(QString::fromUtf8(QByteArray::fromBase64(settings.value("connection/password").toByteArray())));
  m_password->setEchoMode(QLineEdit::Password);
  layout->addRow(tr("&Password:"), m_password);

  buttons = new QDialogButtonBox(Qt::Horizontal, this);
  buttons->addButton(tr("&Connect"), QDialogButtonBox::AcceptRole);
  buttons->addButton(QDialogButtonBox::Close);
  buttons->addButton(QDialogButtonBox::Cancel);
  QObject::connect(buttons, SIGNAL(clicked(QAbstractButton*)), this, SLOT(buttonClicked(QAbstractButton*)));

  layout->addRow(buttons);
}

QString SettingsDialog::server() const
{
  return m_server->text();
}

int SettingsDialog::port() const
{
  return m_port->text().toInt();
}

QString SettingsDialog::username() const
{
  return m_username->text();
}

QString SettingsDialog::password() const
{
  return m_password->text();
}

void SettingsDialog::buttonClicked(QAbstractButton* button)
{
  if(buttons->standardButton(button) == QDialogButtonBox::Cancel) {
    reject();
    return;
  }
  QSettings settings;
  settings.setValue("connection/server", server());
  settings.setValue("connection/port", port());
  settings.setValue("connection/username", username());
  // Very basic roasting just to protect against casual inspection.
  // No algorithm here could protect against a dedicated attacker with access to your hard drive.
  settings.setValue("connection/password", password().toUtf8().toBase64());
  if(buttons->standardButton(button) == QDialogButtonBox::Close) {
    reject();
  } else {
    accept();
  }
}
