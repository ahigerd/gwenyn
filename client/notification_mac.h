#ifndef NOTIFICATION_MAC_H
#define NOTIFICATION_MAC_H

#include <QObject>
#include <QString>

class Notification : public QObject
{
Q_OBJECT
public:
  static Notification* instance();

  inline static void show(const QString& userInfo, const QString& title, const QString& message, const QString& sound = QString()) {
    instance()->showNotification(userInfo, title, message, sound);
  }
  inline static void show(const QString& userInfo, const QString& title, const QString& subtitle, const QString& message, const QString& sound) {
    instance()->showNotification(userInfo, title, subtitle, message, sound);
  }

public slots:
  void showNotification(const QString& userInfo, const QString& title, const QString& message, const QString& sound = QString());
  void showNotification(const QString& userInfo, const QString& title, const QString& subtitle, const QString& message, const QString& sound);

signals:
  void messageClicked(const QString& userInfo);

private:
  friend void Notification_emitMessageClicked(const char* userInfo);
};


#endif
