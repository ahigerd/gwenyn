#include "clientwindow.h"
#include "settingsdialog.h"
#include "chatpane.h"
#include "buddypane.h"
#include "qxtmetaobject.h"
#include "ircserializer.h"
#include <QApplication>
#include <QTabWidget>
#include <QTabBar>
#include <QToolBar>
#include <QAction>
#include <QSettings>
#include <QEventLoop>
#include <QStyle>
#include <QPainter>
#include <QImage>
#include <QPixmap>
#include <QIcon>
#include <QMetaObject>
#include <QMetaEnum>
#ifdef Q_OS_MAC
#include <QFont>
#include "notification_mac.h"
#else
#include <QSystemTrayIcon>
#endif

ClientWindow::ClientWindow(QWidget* parent)
: QMainWindow(parent)
{
  setWindowTitle("Gwenyn");
  setUnifiedTitleAndToolBarOnMac(true);

#ifdef Q_OS_MAC
  // TODO: This is blatantly personal preference for dogfooding.
  // Add a proper font selector.
  setFont(QFont("Lucida Grande"));
#endif

  m_bufferTimer.setInterval(1000);
  m_bufferTimer.setSingleShot(true);
  QObject::connect(&m_bufferTimer, SIGNAL(timeout()), this, SLOT(saveWindowSettings()));

  m_tabs = new QTabWidget(this);
  m_tabs->setTabPosition(QTabWidget::South);
  m_tabs->setTabShape(QTabWidget::Triangular);
  m_tabs->setDocumentMode(true);
  m_tabs->setMovable(true);
  m_tabs->setTabsClosable(true);
  setCentralWidget(m_tabs);
  QObject::connect(m_tabs, SIGNAL(currentChanged(int)), this, SLOT(tabChanged(int)));
  QObject::connect(m_tabs, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));

  m_buddyList = new BuddyPane(this);
  addDockWidget(Qt::RightDockWidgetArea, m_buddyList);
  QObject::connect(m_buddyList, SIGNAL(resized()), &m_bufferTimer, SLOT(start()));
  QObject::connect(m_buddyList, SIGNAL(openChat(QString)), this, SLOT(openChat(QString)));

  m_toolbar = new QToolBar(this);
  m_toolbar->setObjectName("toolbar");
  m_toolbar->setIconSize(QSize(24, 24));
  m_connectAction = m_toolbar->addAction(style()->standardIcon(QStyle::SP_DriveNetIcon), tr("Connect"), this, SLOT(connect()));
  m_settingsAction = m_toolbar->addAction(style()->standardIcon(QStyle::SP_MessageBoxInformation),
#ifdef Q_OS_MAC
      tr("Preferences"),
#else
      tr("Settings"),
#endif
    this, SLOT(showSettings()));
  m_toolbar->addAction(m_buddyList->toggleViewAction());
  addToolBar(Qt::TopToolBarArea, m_toolbar);

#ifdef Q_OS_MAC
  setWindowIcon(QIcon());
  QObject::connect(Notification::instance(), SIGNAL(messageClicked(QString)), this, SLOT(focusTab(QString)));
#else
  m_sysTray = new QSystemTrayIcon(this);
  m_sysTray->setIcon(windowIcon());
  m_sysTray->show();
#endif

  // TODO: temporary implementation
  QObject::connect(&m_client, SIGNAL(partyChat(QString, QString)), this, SLOT(addMessage(QString, QString)));
  QObject::connect(&m_client, SIGNAL(systemMessage(QString)), this, SLOT(systemMessage(QString)));
  QObject::connect(&m_client, SIGNAL(buddyInfo(QString, QString, QString)), this, SLOT(buddyInfo(QString, QString, QString)));
  QObject::connect(&m_client, SIGNAL(buddyInfo(QString, QString, QString)), m_buddyList, SLOT(updateBuddyInfo(QString, QString, QString)));
  QxtMetaObject::connect(&m_client, SIGNAL(connectedToServer()),
      QxtMetaObject::bind(m_connectAction, SLOT(setEnabled(bool)), Q_ARG(bool, false)));
  QxtMetaObject::connect(&m_client, SIGNAL(disconnectedFromServer()),
      QxtMetaObject::bind(m_connectAction, SLOT(setEnabled(bool)), Q_ARG(bool, true)));

  QSettings settings;
  if(settings.contains("ui/geometry")) {
    restoreGeometry(settings.value("ui/geometry").toByteArray());
    restoreState(settings.value("ui/state").toByteArray());
  } else {
    resize(640, 480);
  }

  m_settingsDialog = new SettingsDialog(this);
  QObject::connect(m_settingsDialog, SIGNAL(accepted()), this, SLOT(connect()));
}

bool ClientWindow::event(QEvent* event)
{
  //qDebug() << event->type() << QEvent::staticMetaObject.enumerator(QEvent::staticMetaObject.indexOfEnumerator("Type")).valueToKey(event->type());
  if(event->type() == QEvent::WindowActivate && unreadCount() > 0) {
    if(m_lastUnread.isEmpty()) {
      for(auto pane : m_panes) {
        if(pane->unreadCount() > 0) {
          m_tabs->setCurrentWidget(pane);
          break;
        }
      }
    } else {
      m_tabs->setCurrentWidget(chatPane(m_lastUnread));
    }
  }
  return QMainWindow::event(event);
}

void ClientWindow::showEvent(QShowEvent*)
{
  QTimer::singleShot(0, this, SLOT(showSettings()));
}

void ClientWindow::resizeEvent(QResizeEvent*)
{
  if(!isVisible())
    return;
  m_bufferTimer.start();
}

void ClientWindow::saveWindowSettings()
{
  QSettings settings;
  settings.setValue("ui/geometry", saveGeometry());
  settings.setValue("ui/state", saveState());
}

void ClientWindow::showSettings()
{
  m_settingsDialog->open();
}

void ClientWindow::connect()
{
  QSettings settings;
  QString server = settings.value("connection/server", "").toString();
  int port = settings.value("connection/port", 0).toInt();
  QString username = settings.value("connection/username", "").toString();
  if(username.isEmpty() || server.isEmpty() || port == 0) {
    // TODO: error message
    return;
  }
  m_client.setUsername(username);
  m_client.connect(server, port);
}

ChatPane* ClientWindow::chatPane(const QString& room)
{
  ChatPane* pane = m_panes[room];
  if(!pane) {
    m_panes[room] = pane = new ChatPane(room, this);
    m_tabs->addTab(pane, QString(room).replace("&", "&&"));
    QObject::connect(pane, SIGNAL(sendMessage(QString, QString)), this, SLOT(sendMessage(QString, QString)));
    QObject::connect(pane, SIGNAL(tabLeft()), this, SLOT(tabLeft()));
    QObject::connect(pane, SIGNAL(tabRight()), this, SLOT(tabRight()));
    QObject::connect(pane, SIGNAL(closeTab()), this, SLOT(closeTab()));
    QObject::connect(pane, SIGNAL(markRead()), this, SLOT(markTabRead()));
  }
  return pane;
}

void ClientWindow::systemMessage(const QString& message)
{
  chatPane("&bitlbee")->addMessage("--System--", message);
}

void ClientWindow::addMessage(const QString& sender, const QString& message, bool quiet)
{
  // TODO: better protocol server-side
  QStringList parts = sender.split(":");
  if (parts.length() > 1 && parts[0] == "#19") {
    parts.removeFirst();
    parts[0] = "#19:" + parts[0];
  }
  QString room;
  if(parts.length() == 1 || parts[0][0] == '#' || parts[0][0] == '&' || parts[0][0] == '>') {
    if (parts[0][0] == '>')
      room = parts[0].mid(1);
    else
      room = parts[0];
  } else {
    room = parts[1];
  }
  ChatPane* pane = chatPane(room);
  // XXX: this is ugly on way too many levels
  bool isSelfMessage = parts.last() == QSettings().value("connection/username").toString();
  bool notify = !quiet && (!qApp->activeWindow() || (m_tabs->currentWidget() != pane)) && !isSelfMessage;
  if(notify) {
    m_tabs->setTabIcon(m_tabs->indexOf(pane), style()->standardIcon(QStyle::SP_ArrowRight));
#ifdef Q_OS_MAC
    if(room == parts.last()) {
      Notification::show(room, room, message, "Tink");
    } else {
      Notification::show(room, room, parts.last(), message, "Tink");
    }
#else
    m_sysTray->showMessage(parts.last(), message);
#endif
    QApplication::alert(this);
    m_lastUnread = room;
  }
  pane->addMessage(parts.last(), message, notify);
  if(isSelfMessage) {
    pane->clearUnread();
  }
  updateBadge();
}

void ClientWindow::sendMessage(const QString& target, const QString& message)
{
  // TODO: do this server-side
  if(message.startsWith("/query")) {
    QStringList parts = message.split(' ');
    openChat(parts[1]);
  } else if(message.startsWith("/raw")) {
    m_client.sendPartyChat("bitlbee", message.mid(5));
  } else {
    QString name = target.section(':', 0);
    m_client.sendPartyChat("bitlbee", "PRIVMSG " + name + " :" + message);
  }
}

void ClientWindow::tabLeft()
{
  int index = m_tabs->currentIndex() - 1;
  if(index < 0)
    index = m_tabs->count() - 1;
  m_tabs->setCurrentIndex(index);
}

void ClientWindow::tabRight()
{
  int index = m_tabs->currentIndex() + 1;
  if(index >= m_tabs->count())
    index = 0;
  m_tabs->setCurrentIndex(index);
}

void ClientWindow::closeTab(int index)
{
  if(index < 0) {
    index = m_tabs->currentIndex();
  }
  ChatPane* pane = static_cast<ChatPane*>(m_tabs->widget(index));
  QString label = m_panes.key(pane);
  if(m_lastUnread == label) {
    m_lastUnread.clear();
  }
  m_panes.remove(label);
  m_tabs->removeTab(index);
  pane->deleteLater();
}

void ClientWindow::tabChanged(int index)
{
  m_tabs->setFocus();
  setWindowTitle(tr("Gwenyn - %1").arg(QString(m_tabs->tabText(index)).replace("&&", "&")));
  ChatPane* pane = static_cast<ChatPane*>(m_tabs->currentWidget());
  pane->focusInput();
  markTabRead(pane);
}

void ClientWindow::focusTab(const QString& name)
{
  //qDebug() << "-- focus" << name;

  m_tabs->setCurrentWidget(chatPane(name));
}

void ClientWindow::updateBadge()
{
  int unread = unreadCount();
  QIcon baseIcon(":/gwenyn.png");
  QIcon result;
  if(unread == 0) {
    result = baseIcon;
  } else {
    QImage img = baseIcon.pixmap(512).toImage();
    int size = img.width() / 2.5;
    int pen  = size * 0.1;
    int left = img.width() - size - pen;
    int top  = pen;

    QFont f = font();
    f.setPixelSize(size * 0.8);

    QPainter p(&img);
    p.setFont(f);
    p.setBrush(Qt::red);
    p.setPen(QPen(Qt::white, pen));
    p.drawEllipse(left, top, size, size);
    p.drawText(left, top, size, size, Qt::AlignCenter, QString::number(unread));

    result = QIcon(QPixmap::fromImage(img));
  }
  qApp->setWindowIcon(result);
#ifndef Q_OS_MAC
  setWindowIcon(result);
  m_sysTray->setIcon(result);
#endif
}

int ClientWindow::unreadCount() const
{
  int unread = 0;
  for(auto pane : m_panes) {
    unread += pane->unreadCount();
  }
  return unread;
}

void ClientWindow::markTabRead(ChatPane* pane)
{
  if(!pane) {
    pane = qobject_cast<ChatPane*>(sender());
    if(!pane)
      return;
  }
  m_tabs->setTabIcon(m_tabs->indexOf(pane), QIcon());
  pane->clearUnread();
  updateBadge();
}

void ClientWindow::buddyInfo(const QString& buddy, const QString& status, const QString& statusMessage)
{
  IRCName name = IRCName::fromName(buddy.toUtf8());
  QString nick = QString::fromUtf8(name.nick);
  if(!m_panes.contains(nick)) return;

  QString sender = QString(":%1:%2").arg(nick).arg("--System--");

  if(statusMessage.isNull()) {
    addMessage(sender, QString("\x1D%1 is now %2\x1D").arg(nick, status), true);
  } else {
    addMessage(sender, QString("\x1D%1 is now %2 (%3)\x1D").arg(nick, status, statusMessage), true);
  }
}

void ClientWindow::openChat(const QString& nick)
{
  ChatPane* pane = chatPane(nick);
  m_tabs->setCurrentWidget(pane);
  pane->setFocus();
  pane->focusInput();
}
