#ifndef BUDDYPANE_H
#define BUDDYPANE_H

#include <QDockWidget>
#include <QHash>
class QTreeWidget;
class QTreeWidgetItem;

class BuddyPane : public QDockWidget
{
Q_OBJECT
public:
  BuddyPane(QWidget* parent = 0);

signals:
  void resized();
  void openChat(const QString& nick);

public slots:
  void updateBuddyInfo(const QString& ircname, const QString& status, const QString& message);

protected:
  void resizeEvent(QResizeEvent*);

private slots:
  void itemWasDoubleClicked(QTreeWidgetItem* item, int);

private:
  QTreeWidget* m_list;

  QHash<QString, QTreeWidgetItem*> m_statuses;
  QHash<QString, QTreeWidgetItem*> m_protocols;
  QHash<QString, QTreeWidgetItem*> m_protocolsOffline;
};

#endif
