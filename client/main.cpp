#include <QApplication>
#include <QStyle>
#include "gwenynproto.h"
#include "clientwindow.h"

int main(int argc, char** argv) {
#ifdef Q_OS_MAC
# if QT_VERSION < 0x050000
  // There's some inexplicable flickering with the native renderer on OSX
  QApplication::setGraphicsSystem("raster");
# endif
#endif

  QApplication app(argc, argv);
  app.setApplicationName("Gwenyn");
  app.setOrganizationDomain("alkahest.org");
  app.setOrganizationName("Alkahest");
  app.setWindowIcon(QIcon(":/gwenyn.png"));

  ClientWindow window;
  window.show();

  return app.exec();
}
