#include "notification_mac.h"
#import <Foundation/Foundation.h>

#define NS_QSTRING(s) [NSString stringWithUTF8String:s.toUtf8().constData()]

void Notification_emitMessageClicked(const char* userInfo)
{
  emit Notification::instance()->messageClicked(QString::fromUtf8(userInfo));
}

@interface NotificationDelegate : NSObject <NSUserNotificationCenterDelegate>
{
}

- (BOOL) userNotificationCenter:(NSUserNotificationCenter*)center shouldPresentNotification:(NSUserNotification*)notification;
- (void) userNotificationCenter:(NSUserNotificationCenter*)center didActivateNotification:(NSUserNotification*)notification;

@end

@implementation NotificationDelegate
  - (BOOL) userNotificationCenter:(NSUserNotificationCenter*)center shouldPresentNotification:(NSUserNotification*)notification
  {
    Q_UNUSED(center);
    Q_UNUSED(notification);
    return YES;
  }

  - (void) userNotificationCenter:(NSUserNotificationCenter*)center didActivateNotification:(NSUserNotification*)notification
  {
    Q_UNUSED(center);
    Notification_emitMessageClicked(((NSString*)[notification.userInfo valueForKey:@"userInfo"]).UTF8String);
  }
@end

Notification* Notification::instance()
{
  static Notification object;
  static NotificationDelegate* delegate = [[NotificationDelegate alloc] init];
  static bool initialized = false;

  if(!initialized) {

    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:delegate];
    initialized = true;
  }

  return &object;
}

void Notification::showNotification(const QString& userInfo, const QString& title, const QString& message, const QString& sound)
{
  showNotification(userInfo, title, QString(), message, sound);
}

void Notification::showNotification(const QString& userInfo, const QString& title, const QString& subtitle, const QString& message, const QString& sound)
{
  NSUserNotification *notification = [[NSUserNotification alloc] init];
  NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc] init];
  [userInfoDict setValue:NS_QSTRING(userInfo) forKey:@"userInfo"];
  notification.userInfo = userInfoDict;
  notification.title = NS_QSTRING(title);
  if (!subtitle.isEmpty()) {
    notification.subtitle = NS_QSTRING(subtitle);
  }
  notification.informativeText = NS_QSTRING(message);
  if (sound.isEmpty()) {
    notification.soundName = NSUserNotificationDefaultSoundName;
  } else {
    notification.soundName = NS_QSTRING(sound);
  }

  [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
}
