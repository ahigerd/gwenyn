#ifndef CLIENTWINDOW_H
#define CLIENTWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QHash>
#include "gwenynclient.h"
class QTabWidget;
class BuddyPane;
class ChatPane;
class SettingsDialog;
class QSystemTrayIcon;

class ClientWindow : public QMainWindow
{
Q_OBJECT
public:
  ClientWindow(QWidget* parent = 0);

  int unreadCount() const;

protected:
  bool event(QEvent* event);
  void showEvent(QShowEvent*);
  void resizeEvent(QResizeEvent*);

private slots:
  void connect();
  void showSettings();
  void saveWindowSettings();

  void markTabRead(ChatPane* pane = 0);
  void tabLeft();
  void tabRight();
  void closeTab(int index = -1);
  void tabChanged(int index);
  void focusTab(const QString& name);

  void systemMessage(const QString& message);
  void addMessage(const QString& sender, const QString& message, bool quiet = false);
  void sendMessage(const QString& sender, const QString& message);
  void buddyInfo(const QString& buddy, const QString& status, const QString& statusMessage);
  void openChat(const QString& nick);

private:
  ChatPane* chatPane(const QString& room);
  void updateBadge();

  QTabWidget* m_tabs;
  BuddyPane* m_buddyList;
  GwenynClient m_client;
  QTimer m_bufferTimer;
  QHash<QString, ChatPane*> m_panes;
  QString m_lastUnread;

#ifndef Q_OS_MAC
  QSystemTrayIcon* m_sysTray;
#endif

  QToolBar* m_toolbar;
  QAction* m_connectAction;
  QAction* m_settingsAction;
  SettingsDialog* m_settingsDialog;
};

#endif
