VERSION = 0.0.2

TEMPLATE    = app
DESTDIR     = ..
OBJECTS_DIR = .tmp
MOC_DIR     = .tmp
macx {
    TARGET = Gwenyn
}
else {
    TARGET = gwenyn_client
}
QT       = core gui network
equals(QT_MAJOR_VERSION, 5) {
  QT += widgets
}
CONFIG  += link_prl
HEADERS += settingsdialog.h   gwenynclient.h   chatpane.h   clientwindow.h   buddypane.h
SOURCES += settingsdialog.cpp gwenynclient.cpp chatpane.cpp clientwindow.cpp buddypane.cpp main.cpp
RESOURCES += client.qrc

INCLUDEPATH    += ../common ../common/qxtfork
LIBS           += -L.. -lcommon
unix|win32-g++* {
    PRE_TARGETDEPS += ../libcommon.a
}
else {
    PRE_TARGETDEPS += ../libcommon.lib
}

macx {
    HEADERS += notification_mac.h
    OBJECTIVE_SOURCES += notification_mac.mm
    QMAKE_LFLAGS += -framework Foundation
    ICON = gwenyn.icns
}
win32* {
    RC_FILE = gwenyn.rc
}
