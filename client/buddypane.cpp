#include "buddypane.h"
#include "ircserializer.h"
#include <QTreeWidget>
#include <QAction>
#include <QStyle>
#include <QColor>

BuddyPane::BuddyPane(QWidget* parent)
: QDockWidget(tr("Buddy List"), parent)
{
  setObjectName("buddyList");
  setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  setFeatures(QDockWidget::DockWidgetMovable |
              QDockWidget::DockWidgetFloatable |
              QDockWidget::DockWidgetClosable);
  setWidget(m_list = new QTreeWidget(this));
  toggleViewAction()->setIcon(style()->standardIcon(QStyle::SP_FileDialogListView));
  m_list->clear();
  m_list->setSortingEnabled(false);
  m_list->setColumnCount(2);
  m_list->setHeaderLabels(QStringList() << tr("Name") << tr("ident"));

  QObject::connect(m_list, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(itemWasDoubleClicked(QTreeWidgetItem*, int)));
}

void BuddyPane::updateBuddyInfo(const QString& ircname, const QString& status, const QString& message)
{
  static QHash<QString, QColor> statusColors;
  if(statusColors.isEmpty()) {
    statusColors["Online"] = Qt::green;
    statusColors["Available"] = Qt::green;
    statusColors["Away"] = Qt::yellow;
    statusColors["Offline"] = Qt::red;
    statusColors["Phone"] = Qt::blue;
    statusColors["DND"] = Qt::darkYellow;
  }

  IRCName name = IRCName::fromName(ircname.toUtf8());
  QString protocol = QString::fromUtf8(name.host);
  if(protocol.isEmpty()) return; // TODO: fix this server-side

  QTreeWidgetItem* protocolItem = m_protocols[protocol];
  QTreeWidgetItem* protocolOfflineItem = m_protocolsOffline[protocol];
  if(!protocolItem) {
    m_protocols[protocol] = protocolItem = new QTreeWidgetItem(QStringList() << protocol);
    m_protocolsOffline[protocol] = protocolOfflineItem = new QTreeWidgetItem(QStringList() << tr("Offline"));
    protocolItem->setFirstColumnSpanned(true);
    protocolOfflineItem->setFirstColumnSpanned(true);
    protocolOfflineItem->setData(0, Qt::DecorationRole, QColor(Qt::red));
    m_list->addTopLevelItem(protocolItem);
    protocolItem->setExpanded(true);
    protocolItem->addChild(protocolOfflineItem);
    m_list->invisibleRootItem()->sortChildren(0, Qt::AscendingOrder);
  }

  QString nick = QString::fromUtf8(name.nick);
  if(nick.isEmpty()) return;
  QTreeWidgetItem* buddyItem = m_statuses[nick];
  if(!buddyItem) {
    m_statuses[nick] = buddyItem = new QTreeWidgetItem;
  }
  buddyItem->setData(0, Qt::UserRole, true);
  buddyItem->setData(0, Qt::DisplayRole, nick);
  buddyItem->setData(0, Qt::DecorationRole, statusColors[status]);
  if(!name.ident.isEmpty()) {
    buddyItem->setData(1, Qt::DisplayRole, QString::fromUtf8(name.ident));
  }

  QTreeWidgetItem* targetParent = (status == "Offline" ? protocolOfflineItem : protocolItem);
  if(buddyItem->parent() != targetParent) {
    if(buddyItem->parent())
      buddyItem->parent()->removeChild(buddyItem);
    targetParent->addChild(buddyItem);
    if(targetParent == protocolItem)
      protocolItem->removeChild(protocolOfflineItem);
    targetParent->sortChildren(0, Qt::AscendingOrder);
    if(targetParent == protocolItem)
      protocolItem->insertChild(0, protocolOfflineItem);
    m_list->resizeColumnToContents(0);
  }
}

void BuddyPane::resizeEvent(QResizeEvent*)
{
  emit resized();
}

void BuddyPane::itemWasDoubleClicked(QTreeWidgetItem* item, int)
{
  if(item->data(0, Qt::UserRole).toBool()) {
    emit openChat(item->data(0, Qt::DisplayRole).toString());
  }
}
