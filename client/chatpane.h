#ifndef CHATPANE_H
#define CHATPANE_H

#include <QWidget>
class QTextBrowser;
class QLineEdit;

class ChatPane : public QWidget
{
Q_OBJECT
public:
  ChatPane(const QString& target, QWidget* parent = 0);

  int unreadCount() const;
  void clearUnread();

public slots:
  void focusInput();
  void addMessage(const QString& sender, const QString& message, bool unread = false); // TODO: buddy

protected:
  void showEvent(QShowEvent*);
  bool eventFilter(QObject* obj, QEvent* event);

signals:
  void tabLeft();
  void tabRight();
  void closeTab();
  void markRead();
  void sendMessage(const QString& recipient, const QString& message);

private slots:
  void sendMessage();

private:
  QTextBrowser* m_log; // TODO: QWebView?
  QLineEdit* m_message;
  QString m_target;
  int m_unread;
};

#endif
