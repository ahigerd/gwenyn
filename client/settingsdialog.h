#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
class QDialogButtonBox;
class QAbstractButton;
class QLineEdit;

class SettingsDialog : public QDialog
{
Q_OBJECT
public:
  SettingsDialog(QWidget* parent = 0);

  QString server() const;
  int port() const;
  QString username() const;
  QString password() const;

private slots:
  void buttonClicked(QAbstractButton* button);

private:
  // TODO: host locally
  QDialogButtonBox* buttons;
  QLineEdit* m_server;
  QLineEdit* m_port;
  QLineEdit* m_username;
  QLineEdit* m_password;
};

#endif
