#include "gwenynrpc.h"
#include "gwenynproto.h"
#include "qxtrpcservice.h"
#include <QTimerEvent>
#include <QtDebug>

AbstractCallInfo::AbstractCallInfo(const QString& functionName, const QList<qulonglong>& ids)
: QObject(0), cookie(0), timerID(0), _ids(ids), _functionName(functionName)
{
  setTimeout(10000);
}

AbstractCallInfo::~AbstractCallInfo()
{
  stopTimer();
}

QString AbstractCallInfo::functionName() const
{
  return _functionName;
}

void AbstractCallInfo::stopTimer()
{
  if(timerID) {
    killTimer(timerID);
  }
  timerID = 0;
}

void AbstractCallInfo::setTimeout(int ms)
{
  stopTimer();
  timerID = startTimer(ms);
}

void AbstractCallInfo::timerEvent(QTimerEvent*)
{
  error(Gwenyn::Timeout, QVariantList());
}

void AbstractCallInfo::abort()
{
  error(Gwenyn::Aborted, QVariantList());
}

void AbstractCallInfo::error(int errorCode, const QVariantList& params)
{
  stopTimer();
  if(errorCB) {
    errorCB(this, errorCode, params);
  }
}

void AbstractCallInfo::invoke()
{
  QVariantList args = argumentList();
  QxtRPCService* service = Gwenyn::instance()->m_rpc;
  cookie = Gwenyn::instance()->nextCookie++;
  if(_ids.isEmpty()) {
    service->call(
        _functionName,
        cookie,
        args.value(0),
        args.value(1),
        args.value(2),
        args.value(3),
        args.value(4),
        args.value(5),
        args.value(6));
  } else {
    service->call(
        _ids,
        _functionName,
        cookie,
        args.value(0),
        args.value(1),
        args.value(2),
        args.value(3),
        args.value(4),
        args.value(5),
        args.value(6));
  }
}

GwenynRPCBase::GwenynRPCBase(const QString& name)
: QObject(nullptr), functionName(name)
{
  Gwenyn* g = Gwenyn::instance();
  QObject::connect(g, SIGNAL(shouldAbortAllRPCs()), this, SLOT(abortAllRPCs()));
  QObject::connect(g, SIGNAL(shouldRefreshAllTimeouts()), this, SLOT(refreshAllTimeouts()));

  if (g->isServer) {
    g->m_rpc->attachSlot(functionName, this,
        SLOT(invokedByClient(qulonglong, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant)));
    g->m_rpc->attachSlot(functionName + "_result", this,
        SLOT(gotResult(qulonglong, qulonglong, QVariant)));
    g->m_rpc->attachSlot(functionName + "_ack", this,
        SLOT(gotAck(qulonglong, qulonglong)));
    g->m_rpc->attachSlot(functionName + "_error", this,
        SLOT(gotError(qulonglong, qulonglong, int, QVariantList)));
  } else {
    g->m_rpc->attachSlot(functionName, this,
        SLOT(invokedByServer(QVariant, QVariant, QVariant, QVariant, QVariant, QVariant, QVariant)));
    g->m_rpc->attachSlot(functionName + "_result", this,
        SLOT(gotResult(qulonglong, QVariant)));
    g->m_rpc->attachSlot(functionName + "_ack", this,
        SLOT(gotAck(qulonglong)));
    g->m_rpc->attachSlot(functionName + "_error", this,
        SLOT(gotError(qulonglong, int, QVariantList)));
  }
}

void GwenynRPCBase::result(qulonglong cookie, const QVariant& value)
{
  Gwenyn::instance()->m_rpc->call(functionName + "_result", cookie, value);
}

void GwenynRPCBase::ack(qulonglong cookie)
{
  Gwenyn::instance()->m_rpc->call(functionName + "_ack", cookie);
}

void GwenynRPCBase::error(qulonglong cookie, int errorCode, const QVariantList& params)
{
  Gwenyn::instance()->m_rpc->call(functionName + "_error", cookie, errorCode, params);
}

void GwenynRPCBase::refreshAllTimeouts()
{
  for(auto rpc : pendingRPCs) {
    rpc->setTimeout(5000);
  }
}

void GwenynRPCBase::abortAllRPCs()
{
  for(auto rpc : pendingRPCs) {
    rpc->abort();
    delete rpc;
  }
  pendingRPCs.clear();
}

void GwenynRPCBase::gotResult(qulonglong cookie, const QVariant& value)
{
  AbstractCallInfo* rpc = pendingRPCs.take(cookie);
  if(!rpc) return;
  Gwenyn::instance()->refreshAllTimeouts();
  rpc->response(value);
  delete rpc;
}

void GwenynRPCBase::gotAck(qulonglong cookie)
{
  gotResult(cookie, QVariant());
}

void GwenynRPCBase::gotError(qulonglong cookie, int errorCode, const QVariantList& params)
{
  AbstractCallInfo* rpc = pendingRPCs.take(cookie);
  if(!rpc) return;
  if(errorCode != Gwenyn::Timeout && errorCode != Gwenyn::Aborted) {
    // Timing out shouldn't refresh timeouts
    Gwenyn::instance()->refreshAllTimeouts();
  }
  rpc->error(errorCode, params);
  delete rpc;
}

void GwenynRPCBase::gotResult(qulonglong, qulonglong cookie, const QVariant& value)
{
  gotResult(cookie, value);
}

void GwenynRPCBase::gotAck(qulonglong, qulonglong cookie)
{
  gotResult(cookie, QVariant());
}

void GwenynRPCBase::gotError(qulonglong, qulonglong cookie, int errorCode, const QVariantList& params)
{
  gotError(cookie, errorCode, params);
}
