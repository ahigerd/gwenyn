#ifndef VARIANTLIST_H
#define VARIANTLIST_H

#include <QVariant>
#include <tuple>

extern void packVariantList_worker(QVariantList&);

template<typename First, typename... Args>
inline void packVariantList_worker(QVariantList& variants, const First& variant, Args&&... args)
{
  variants << QVariant::fromValue(variant);
  packVariantList_worker(variants, args...);
}

template<typename... Args>
QVariantList packVariantList(Args&&... args)
{
  QVariantList variants;
  packVariantList_worker(variants, args...);
  return variants;
}

template<int index, typename TUPLE, bool finished = (index >= std::tuple_size<TUPLE>::value)>
struct packVariantList_tuple
{
  static inline void add(QVariantList& variants, const TUPLE& args)
  {
    variants << QVariant::fromValue(std::get<index>(args));
    packVariantList_tuple<index + 1, TUPLE>::add(variants, args);
  }
};

template<int index, typename TUPLE>
struct packVariantList_tuple<index, TUPLE, true>
{
  static inline void add(QVariantList&, const TUPLE&)
  {
    // no-op
  }
};

template<typename... Args>
QVariantList packVariantList(const std::tuple<Args...>& args)
{
  QVariantList variants;
  packVariantList_tuple<0, std::tuple<Args...>>::add(variants, args);
  return variants;
}

template<typename T, typename... Args>
struct packTuple_worker
{
  std::tuple<T, Args...> make(const QVariantList& variants, int index = 0)
  {
    return std::tuple_cat(std::tuple<T>(variants[index].value<T>(), packTuple_worker<Args...>::make(variants, index + 1)));
  }
};

template<typename T>
struct packTuple_worker<T>
{
  std::tuple<> make(const QVariantList&, int)
  {
    return std::tuple<>();
  }
};

template<typename... Args>
std::tuple<Args...> packTuple(const QVariantList& variants)
{
  return packTuple_worker<Args...>::make(variants);
}

#endif
