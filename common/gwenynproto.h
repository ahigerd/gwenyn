#ifndef GWENYNPROTO_H
#define GWENYNPROTO_H

#include <QMetaType>
#include <QString>
#include <QDateTime>
#include <tuple>
#include "variantlist.h"
#include "gwenynrpc.h"
class QxtRPCService;

#define GWENYN_METATYPE_BASE 16384

#define GWENYN_DECLARE_METATYPE(TYPE, METATYPEID) \
  template<> struct QMetaTypeId2<TYPE> { \
    enum { Defined = 1, IsBuiltIn = false, MetaType = GWENYN_METATYPE_BASE + METATYPEID }; \
    static inline constexpr int qt_metatype_id() { return GWENYN_METATYPE_BASE + METATYPEID; } \
  };

struct Buddy
{
  QByteArray nick;
  QByteArray account;
  QByteArray ident;
  QString displayName;
};
GWENYN_DECLARE_METATYPE(Buddy, 1);
QDataStream& operator<<(QDataStream& out, const Buddy& buddy);
QDataStream& operator>>(QDataStream& in, Buddy& buddy);

class GwenynBase : public QObject
{
protected:
  GwenynBase(QxtRPCService* rpc, bool isServer);

  QxtRPCService* m_rpc;
  qulonglong nextCookie;
  bool isServer;
};

class Gwenyn : public GwenynBase
{
Q_OBJECT
public:
  enum ErrorCode {
    Timeout,
    UserNotFound,
    IdentAlreadySet,
    Aborted,
    NoSuchAccount,
  };

  static QString error(int err);
  static QString error(int err, const QVariantList& params);
  static void registerMetaTypes();

  enum BuddyState
  {
    Offline,
    Available,
    Away
  };

  typedef QMap<QString, QString> StringMap;

  Gwenyn(QxtRPCService* rpc, bool isServer);

  GwenynRPC<QString> requestUsername{"requestUsername"};
  GwenynRPC<QString> requestPassword{"requestPassword"};
  GwenynRPC<void> ping{"ping"};
  GwenynRPC<void, QString, QString> partyChat{"partyChat"};
  GwenynRPC<void, QString> accountConnected{"accountConnected"};
  GwenynRPC<void, QString, QString> accountDisconnected{"accountDisconnected"};
  GwenynRPC<void, QString, StringMap> accountBuddyList{"accountBuddyList"};
  GwenynRPC<void, QString, QString, QString> buddyStatus{"buddyStatus"};
  GwenynRPC<void, QString, QString, QString> buddyMessage{"buddyMessage"};

  static Gwenyn* instance();

public slots:
  void abortAllRPCs();
  void refreshAllTimeouts();

signals:
  void shouldAbortAllRPCs();
  void shouldRefreshAllTimeouts();

protected:
  friend class GwenynRPCBase;
  friend class AbstractCallInfo;
};
GWENYN_DECLARE_METATYPE(Gwenyn::StringMap, 2);

#define GwenynProto (*Gwenyn::instance())

#endif
