#ifndef GWENYNRPC_H
#define GWENYNRPC_H

#include <QObject>
#include <QtDebug>
#include <tuple>
#include <functional>
#include "variantlist.h"
class QxtRPCService;
class AbstractCallInfo;

#define RPC_ERROR_CALLBACK(CLASS, NAME) \
  void NAME(AbstractCallInfo* callInfo, int errorCode, const QVariantList& params); \
  AbstractCallInfo::ErrorCallback bind_ ## NAME = std::bind(&CLASS::NAME, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)


template <typename ReturnType, typename... ParamTypes>
class GwenynRPC;

template <typename ReturnType, typename... ParamTypes>
class CallInfo;

template <typename ReturnType>
struct CallInfoResultCallback {
  using type = std::function<void(AbstractCallInfo*, ReturnType)>;
};

template <>
struct CallInfoResultCallback<void> {
  using type = std::function<void(AbstractCallInfo*)>;
};

class AbstractCallInfo : public QObject
{
Q_OBJECT
public:
  using ErrorCallback = std::function<void(AbstractCallInfo*, int, const QVariantList&)>;

  template<typename ReturnType, typename... ParamTypes>
  CallInfo<ReturnType, ParamTypes...>* cast(const GwenynRPC<ReturnType, ParamTypes...>& rpc)
  {
    if(rpc.functionName == _functionName)
      return static_cast<CallInfo<ReturnType, ParamTypes...>*>(this);
    return nullptr;
  }

  QString functionName() const;
  virtual QVariantList argumentList() const = 0;

  void abort();

  void setTimeout(int ms);

protected:
  friend class GwenynRPCBase;

  AbstractCallInfo(const QString& functionName, const QList<qulonglong>& ids = QList<qulonglong>());
  ~AbstractCallInfo();

  void invoke();

  void timerEvent(QTimerEvent* event);
  virtual void response(const QVariant& value) = 0;
  void error(int errorCode, const QVariantList& params);
  void stopTimer();

  qulonglong cookie;
  ErrorCallback errorCB;
  int timerID;

  QList<qulonglong> _ids;
  QString _functionName;
};

template <typename ReturnType, typename... ParamTypes>
class CallInfoBase : public AbstractCallInfo
{
public:
  friend class GwenynRPC<ReturnType, ParamTypes...>;
  using ParamTuple = std::tuple<ParamTypes...>;
  using This = CallInfo<ReturnType, ParamTypes...>;
  using ResultCallback = typename CallInfoResultCallback<ReturnType>::type;

  template <int i>
  typename std::tuple_element<i, ParamTuple>::type param() const
  {
    return std::get<i>(params);
  }

  virtual QVariantList argumentList() const
  {
    return packVariantList(params);
  }

  This& then(ResultCallback cb)
  {
    thenCB = cb;
    return *static_cast<This*>(this);
  }

  This& except(ErrorCallback cb)
  {
    errorCB = cb;
    return *static_cast<This*>(this);
  }

protected:
  ParamTuple params;
  ResultCallback thenCB;

  CallInfoBase(const QString& name, const ParamTypes&... args)
  : AbstractCallInfo(name), params(std::make_tuple(args...))
  {
    // initializers only
  }

  CallInfoBase(const QList<qulonglong>& ids, const QString& name, const ParamTypes&... args)
  : AbstractCallInfo(name, ids), params(std::make_tuple(args...))
  {
    // initializers only
  }

  CallInfoBase(const QList<qulonglong>& ids, const QString& name, const std::tuple<ParamTypes...>& args)
  : AbstractCallInfo(name, ids), params(args)
  {
    // initializers only
  }
};

template <typename ReturnType, typename... ParamTypes>
class CallInfo : public CallInfoBase<ReturnType, ParamTypes...>
{
protected:
  friend class GwenynRPC<ReturnType, ParamTypes...>;
  using CallInfoBase<ReturnType, ParamTypes...>::CallInfoBase;

  virtual void response(const QVariant& value) {
    this->stopTimer();
    if(this->thenCB) {
      this->thenCB(this, qvariant_cast<ReturnType>(value));
    }
  }
};

template <typename... ParamTypes>
struct CallInfo<void, ParamTypes...> : CallInfoBase<void, ParamTypes...>
{
protected:
  friend class GwenynRPC<void, ParamTypes...>;
  using CallInfoBase<void, ParamTypes...>::CallInfoBase;

  virtual void response(const QVariant&) {
    this->stopTimer();
    if(this->thenCB) {
      this->thenCB(this);
    }
  }
};

class GwenynRPCBase : public QObject
{
Q_OBJECT
protected:
  friend class AbstractCallInfo;
  GwenynRPCBase(const QString& name);

public slots:
  void result(qulonglong cookie, const QVariant& value);
  void ack(qulonglong cookie);
  void error(qulonglong cookie, int errorCode, const QVariantList& params);

  void refreshAllTimeouts();
  void abortAllRPCs();

public:
  template <typename... Args>
  inline void error(qulonglong cookie, int errorCode, Args&&... args) {
    error(cookie, errorCode, packVariantList(args...));
  }

private slots:
  void gotResult(qulonglong cookie, const QVariant& value);
  void gotAck(qulonglong cookie);
  void gotError(qulonglong cookie, int errorCode, const QVariantList& params);

  void gotResult(qulonglong id, qulonglong cookie, const QVariant& value);
  void gotAck(qulonglong id, qulonglong cookie);
  void gotError(qulonglong id, qulonglong cookie, int errorCode, const QVariantList& params);

protected slots:
  virtual void invokedByServer(const QVariant& cookie, const QVariant& p1 = QVariant(), const QVariant& p2 = QVariant(), const QVariant& p3 = QVariant(),
      const QVariant& p4 = QVariant(), const QVariant& p5 = QVariant(), const QVariant& p6 = QVariant()) = 0;
  virtual void invokedByClient(qulonglong id, const QVariant& cookie, const QVariant& p1 = QVariant(), const QVariant& p2 = QVariant(),
      const QVariant& p3 = QVariant(), const QVariant& p4 = QVariant(), const QVariant& p5 = QVariant(), const QVariant& p6 = QVariant()) = 0;

protected:
  QString functionName;

  template <typename T>
  T track(T rpc)
  {
    pendingRPCs[rpc->cookie] = rpc;
    return rpc;
  }

  QHash<qulonglong, AbstractCallInfo*> pendingRPCs;
};

template <typename T>
class GwenynRPCResultFn : public GwenynRPCBase
{
protected:
  using GwenynRPCBase::GwenynRPCBase;
  enum { isVoid = false };

public:
  void result(qulonglong cookie, T&& value) {
    GwenynRPCBase::result(cookie, QVariant::fromValue(value));
  }
};

template <>
struct GwenynRPCResultFn<void> : public GwenynRPCBase
{
protected:
  using GwenynRPCBase::GwenynRPCBase;
  enum { isVoid = true };

public:
  void ack(qulonglong cookie) {
    result(cookie, QVariant());
  }
};

template <typename ReturnType, typename... ParamTypes>
class GwenynRPC : public GwenynRPCResultFn<ReturnType>
{
  using This = GwenynRPC<ReturnType, ParamTypes...>;
  using ClientFN = std::function<void(qulonglong, ParamTypes...)>;
  using ServerFN = std::function<void(qulonglong, qulonglong, ParamTypes...)>;

public:
  GwenynRPC(const QString& name)
  : GwenynRPCResultFn<ReturnType>(name), clientFunction(0), serverFunction(0)
  {
    // initializers only
  }

  using CallInfo = ::CallInfo<ReturnType, ParamTypes...>;

  CallInfo* operator()(const ParamTypes&... args)
  {
    CallInfo* callInfo = new CallInfo(this->functionName, args...);
    callInfo->invoke();
    return this->track(callInfo);
  }

  CallInfo* operator()(const QList<qulonglong>& ids, const ParamTypes&... args)
  {
    CallInfo* callInfo = new CallInfo(ids, this->functionName, args...);
    callInfo->invoke();
    return this->track(callInfo);
  }

  CallInfo* operator()(qulonglong id, const ParamTypes&... args)
  {
    return (*this)(QList<qulonglong>() << id, args...);
  }

  void bind(const ClientFN& fn)
  {
    clientFunction = fn;
  }

  void bind(const ServerFN& fn)
  {
    serverFunction = fn;
  }

protected:
  void invokedByServer(const QVariant& cookie, const QVariant& p1, const QVariant& p2, const QVariant& p3,
      const QVariant& p4, const QVariant& p5, const QVariant& p6)
  {
    if(!clientFunction) {
      qWarning() << "GwenynRPC: No handler bound for" << this->functionName;
      return;
    }
    invoke<ClientFN, ParamTypes...>(
        clientFunction, qvariant_cast<qulonglong>(cookie),
        QVariantList() << p1 << p2 << p3 << p4 << p5 << p6);
  }

  void invokedByClient(qulonglong id, const QVariant& cookie, const QVariant& p1, const QVariant& p2,
      const QVariant& p3, const QVariant& p4, const QVariant& p5, const QVariant& p6)
  {
    if(!serverFunction) {
      qWarning() << "GwenynRPC: No handler bound for" << this->functionName;
      return;
    }
    invoke<ServerFN, qulonglong, ParamTypes...>(
        serverFunction, qvariant_cast<qulonglong>(id),
        QVariantList() << cookie << p1 << p2 << p3 << p4 << p5 << p6);
  }

private:
  ClientFN clientFunction;
  ServerFN serverFunction;

  template <typename FN>
  void invoke(const FN& fn, qulonglong p1, const QVariantList&)
  {
    fn(p1);
  }

  template <typename FN, typename T1>
  void invoke(const FN& fn, qulonglong p1, const QVariantList& args)
  {
    fn(p1, qvariant_cast<T1>(args[0]));
  }

  template <typename FN, typename T1, typename T2>
  void invoke(const FN& fn, qulonglong p1, const QVariantList& args)
  {
    fn(p1, qvariant_cast<T1>(args[0]), qvariant_cast<T2>(args[1]));
  }

  template <typename FN, typename T1, typename T2, typename T3>
  void invoke(const FN& fn, qulonglong p1, const QVariantList& args)
  {
    fn(p1, qvariant_cast<T1>(args[0]), qvariant_cast<T2>(args[1]), qvariant_cast<T3>(args[2]));
  }

  template <typename FN, typename T1, typename T2, typename T3, typename T4>
  void invoke(const FN& fn, qulonglong p1, const QVariantList& args)
  {
    fn(p1, qvariant_cast<T1>(args[0]), qvariant_cast<T2>(args[1]), qvariant_cast<T3>(args[2]),
        qvariant_cast<T4>(args[3]));
  }

  template <typename FN, typename T1, typename T2, typename T3, typename T4, typename T5>
  void invoke(const FN& fn, qulonglong p1, const QVariantList& args)
  {
    fn(p1, qvariant_cast<T1>(args[0]), qvariant_cast<T2>(args[1]), qvariant_cast<T3>(args[2]),
        qvariant_cast<T4>(args[3]), qvariant_cast<T5>(args[4]));
  }

  template <typename FN, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
  void invoke(const FN& fn, qulonglong p1, const QVariantList& args)
  {
    fn(p1, qvariant_cast<T1>(args[0]), qvariant_cast<T2>(args[1]), qvariant_cast<T3>(args[2]),
        qvariant_cast<T4>(args[3]), qvariant_cast<T5>(args[4]), qvariant_cast<T6>(args[5]));
  }
};

#endif
