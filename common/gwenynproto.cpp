#include "gwenynproto.h"
#include <map>
#include <QDataStream>

/*

RPC protocol:

C: void ident(cookie, QString username, QString password)

C: void ping(cookie)
-- TODO: server expiration of connections -- necessary?

CS: void partyChat(cookie, QString receiverName, QString message)
-- TODO: timestamp
-- TODO: HTML message
-- TODO: delivery confirmation

S: void buddyState(cookie, QString ircname, QString status, QString statusMessage)
-- TODO: contact object instead of ircname
-- TODO: enum instead of status string
-- TODO: extensible object instead of multiple params (QVariantMap?)

S: void ack(cookie)
-- TODO: optional return value
-- TODO: C

S: void nack(cookie, enum error, QVariantList params)
-- TODO: C


TODO:

CS: void buddyMessage(cookie, peer, message)
CS: void groupMessage(cookie, room, peer, message)
-- peer is recipient for C, sender for S
-- add timestamp for S
-- TODO: HTML message
-- ???: Continue to treat Twitter as group, or specialize?
---- If specialize, specialize in client only, or on server?

C: BuddyInfo requestBuddyInfo(cookie, ident)

S: QString requestUsername()
S: QString requestPassword()

*/

void packVariantList_worker(QVariantList&) { /* no-op */ }

QDataStream& operator<<(QDataStream& out, const Buddy& buddy)
{
    out << buddy.nick;
    out << buddy.account;
    out << buddy.ident;
    out << buddy.displayName;
    return out;
}

QDataStream& operator>>(QDataStream& in, Buddy& buddy)
{
    in >> buddy.nick;
    in >> buddy.account;
    in >> buddy.ident;
    in >> buddy.displayName;
    return in;
}

static const std::map<int, QString> errors = {
  { Gwenyn::UserNotFound, QT_TRANSLATE_NOOP("Gwenyn", "User \"%1\" not found") },
  { Gwenyn::IdentAlreadySet, QT_TRANSLATE_NOOP("Gwenyn", "Cannot set identity at this time") },
  { Gwenyn::Timeout, QT_TRANSLATE_NOOP("Gwenyn", "Request timeout") },
  { Gwenyn::Aborted, QT_TRANSLATE_NOOP("Gwenyn", "Aborted by caller") },
};

QString Gwenyn::error(int err)
{
  try {
    return errors.at(err);
  } catch(const std::out_of_range&) {
    return "Unknown error";
  }
}

QString Gwenyn::error(int err, const QVariantList& params)
{
  int len = params.length();
  QString msg = error(err);
  for(int i = 0; i < len; i++) {
    msg = msg.arg(params.value(i).toString());
  }
  return msg;
}

void Gwenyn::registerMetaTypes()
{
    qRegisterMetaType<Buddy>("Buddy");
    qRegisterMetaTypeStreamOperators<Buddy>("Buddy");
    qRegisterMetaType<Gwenyn::StringMap>();
    qRegisterMetaTypeStreamOperators<Gwenyn::StringMap>();
}

static GwenynBase* gwenynInstance = 0;

GwenynBase::GwenynBase(QxtRPCService* rpc, bool isServer)
: QObject(0), m_rpc(rpc), nextCookie(0), isServer(isServer)
{
  gwenynInstance = this;
}

Gwenyn::Gwenyn(QxtRPCService* rpc, bool isServer)
: GwenynBase(rpc, isServer)
{
  registerMetaTypes();
}

void Gwenyn::abortAllRPCs()
{
  emit shouldAbortAllRPCs();
}

void Gwenyn::refreshAllTimeouts()
{
  emit shouldRefreshAllTimeouts();
}

Gwenyn* Gwenyn::instance()
{
  return static_cast<Gwenyn*>(gwenynInstance);
}
