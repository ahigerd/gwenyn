TEMPLATE     = lib
CONFIG      += staticlib create_prl
DESTDIR      = ../
OBJECTS_DIR  = .tmp
MOC_DIR      = .tmp
TARGET       = common
QT           = core network
INCLUDEPATH += qxtfork/

HEADERS = ircserializer.h   gwenynproto.h   gwenynrpc.h
SOURCES = ircserializer.cpp gwenynproto.cpp gwenynrpc.cpp

HEADERS += qxtfork/qxtrpcservice.h   qxtfork/qxtrpcpeer.h   qxtfork/qxtsslserver.h   qxtfork/qxtsslconnectionmanager.h
SOURCES += qxtfork/qxtrpcservice.cpp qxtfork/qxtrpcpeer.cpp qxtfork/qxtsslserver.cpp qxtfork/qxtsslconnectionmanager.cpp

HEADERS += qxtfork/qxtabstractconnectionmanager.h   qxtfork/qxttcpconnectionmanager.h   qxtfork/qxtdatastreamsignalserializer.h
SOURCES += qxtfork/qxtabstractconnectionmanager.cpp qxtfork/qxttcpconnectionmanager.cpp qxtfork/qxtdatastreamsignalserializer.cpp

HEADERS += qxtfork/qxtdaemon.h   qxtfork/qxtmetaobject.h   qxtfork/qxtcommandoptions.h   qxtfork/qxtstdio.h   qxtfork/qxtpipe.h
SOURCES += qxtfork/qxtdaemon.cpp qxtfork/qxtmetaobject.cpp qxtfork/qxtcommandoptions.cpp qxtfork/qxtstdio.cpp qxtfork/qxtpipe.cpp

HEADERS += qxtfork/qxtglobal.h qxtfork/qxtrpcservice_p.h qxtfork/qxttcpconnectionmanager_p.h qxtfork/qxtabstractsignalserializer.h
HEADERS += qxtfork/qxtboundfunction.h qxtfork/qxtboundcfunction.h qxtfork/qxtboundfunctionbase.h qxtfork/qxtmetatype.h
HEADERS += qxtfork/qxtnullable.h qxtfork/qxtnull.h qxtfork/qxtstdio_p.h qxtfork/qxtpipe_p.h
