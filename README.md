Gwenyn
======

Gwenyn is a bitlbee/libpurple-based multiprotocol chat system.

The purpose of Gwenyn is to centralize presence handling and manage connectivity across multiple devices and chat
services while providing centralized logging and a unified feature set. Gwenyn's goal is to avoid losing messages on
realtime-only chat services in the face of unstable mobile network connections and to ensure that all messages are
captured on all devices instead of having them sent to different places when more than one device is logged in at the
same time.

The implementation as it stands does not achieve the goals due to lack of available time to dedicate to the project.
Most notably, logging is not implemented at all, and the server software is not bug-free, but the system as a whole
is sufficiently complete for day-to-day use by the author.