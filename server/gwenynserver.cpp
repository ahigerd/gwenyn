#include "gwenynserver.h"
#include "keyfiles.h"
#include "qxtabstractconnectionmanager.h"
#include "qxtsslconnectionmanager.h"
#include "qxtmetaobject.h"
#include "qxtdatastreamsignalserializer.h"
#include <QtDebug>

GwenynServer::GwenynServer(QObject* parent)
: QxtRPCPeer(parent), gwenynInstance(this, true)
{
  static_cast<QxtDataStreamSignalSerializer*>(serializer())->setDataStreamVersion(12);

  // TODO: might need to twiddle with this for Windows
  QxtSslConnectionManager* ssl = new QxtSslConnectionManager(this);
  SslKeyPair certs = getCertificates(true);
  ssl->setLocalCertificate(certs.first);
  ssl->setPrivateKey(certs.second);
  setConnectionManager(ssl);
  using namespace std::placeholders;
  GwenynProto.partyChat.bind(std::bind(&GwenynServer::partyChat, this, _1, _2, _3, _4));
  GwenynProto.ping.bind(std::bind(&GwenynServer::pong, this, _2));
  attachSlot("ident", this, SLOT(setUsername(qulonglong, qulonglong, QString, QString)));
  QObject::connect(connectionManager(), SIGNAL(newConnection(QIODevice*, qulonglong)),
                   this, SLOT(newConnection(QIODevice*, qulonglong)));
  QObject::connect(connectionManager(), SIGNAL(disconnected(QIODevice*, qulonglong)),
                   this, SLOT(disconnected(QIODevice*, qulonglong)));
}

void GwenynServer::newConnection(QIODevice*, qulonglong connID)
{
  pendingConnections.insert(connID);
  qDebug() << "connected" << connID;
}

void GwenynServer::disconnected(QIODevice*, qulonglong connID)
{
  qDebug() << "disconnected" << connID;
  if(pendingConnections.contains(connID)) {
    // Disconnected before saying who it was
    pendingConnections.remove(connID);
    return;
  }
  if(!usersByConnection.contains(connID)) {
    // Sanity check: A spurious disconnected signal was received for a user that isn't connected.
    return;
  }
  GwenynUser* user = usersByConnection[connID];
  user->connections.removeAll(connID);
  usersByConnection.remove(connID);
  if(!user->useBouncer && user->connections.isEmpty()) {
    users.remove(user->name);
    delete user;
    user = 0;
  }
}

void GwenynServer::setUsername(qulonglong connID, qulonglong cookie, const QString& name, const QString& password)
{
  qDebug() << "setUsername";
  if(usersByConnection.contains(connID)) {
    qDebug() << "connID already identified";
    nack(connID, cookie, Gwenyn::IdentAlreadySet);
    return;
  }
  if(!pendingConnections.contains(connID)) {
    // Sanity check: A spurious signal was received for a user that isn't connected.
    qDebug() << "spurious";
    return;
  }
  pendingConnections.remove(connID);
  GwenynUser* user;
  if(!users.contains(name)) {
    users[name] = user = new GwenynUser(name);
    // TODO: this is really stupid
    user->bitlbee.setIdent(name, password);
    QxtMetaObject::connect(&user->bitlbee, SIGNAL(receivedMessage(IRCName, QByteArray, QByteArray)),
        QxtMetaObject::bind(this, SLOT(userPrivMsg(QString, IRCName, QByteArray, QByteArray)),
          Q_ARG(QString, name), QXT_BIND(1), QXT_BIND(2), QXT_BIND(3)));
    QxtMetaObject::connect(&user->bitlbee, SIGNAL(buddyState(IRCName, QString, QString)),
        QxtMetaObject::bind(this, SLOT(userBuddyState(QString, IRCName, QString, QString)),
          Q_ARG(QString, name), QXT_BIND(1), QXT_BIND(2), QXT_BIND(3)));
  } else {
    user = users[name];
  }
  user->connections.append(connID);
  usersByConnection[connID] = user;
  qDebug() << "identified" << connID << name;
  ack(connID, cookie);
  // TODO: this is fragile: what if ident fails? refactor
  for(auto message : user->queuedMessages) {
    //call(user->connections, "partyChat", message.sender, message.message);
    GwenynProto.partyChat(user->connections, message.sender, message.message);
  }
  user->queuedMessages.clear();
}

void GwenynServer::partyChat(qulonglong connID, qulonglong cookie, const QString& recipient, const QString& message)
{
  if (recipient == "bitlbee") {
    GwenynUser* user = usersByConnection[connID];
    if (message == "connect") {
      if(!user->bitlbee.isConnected()) {
        user->connect();
      } else {
        //call(connID, "partyChat", "&bitlbee", "Connected");
        GwenynProto.partyChat(connID, "&bitlbee", "Connected");
        user->bitlbee.requestBuddyList();
      }
      ack(connID, cookie);
      return;
    }
    qDebug() << message;
    // TODO: buddy IDs need account IDs
    QString content = message.section(' ', 2);
    if(content.startsWith(':')) {
      content = content.mid(1);
    }
    user->bitlbee.sendMessage(cookie, "", message.section(' ', 1, 1), content);
    //user->bitlbee.sendRaw(message);
    //ack(connID, cookie);
    //GwenynProto.partyChat.ack(cookie);
    return;
  }

  if(!users.contains(recipient)) {
    //nack(connID, cookie, Gwenyn::UserNotFound, recipient);
    GwenynProto.partyChat.error(cookie, Gwenyn::UserNotFound, recipient);
    return;
  }
  GwenynUser* user = usersByConnection[connID];
  GwenynUser* target = users[recipient];
  QList<qulonglong> conns = target->connections + user->connections;
  conns.removeAll(connID);
  //call(conns, "partyChat", user->name, message);
  GwenynProto.partyChat(connID, user->name, message);
  if(target->connections.isEmpty()) {
    target->queuedMessages << (GwenynUser::QueuedMessage){ user->name, target->name, message };
  }
  if(user->connections.isEmpty()) {
    // TODO: evaluate?
  }
  //ack(connID, cookie);
  GwenynProto.partyChat.ack(cookie);
}

void GwenynServer::ack(qulonglong connID, qulonglong cookie)
{
  call(connID, "ack", cookie);
}

void GwenynServer::pong(qulonglong cookie)
{
  GwenynProto.ping.ack(cookie);
}

template <typename... Args>
void GwenynServer::nack(qulonglong connID, qulonglong cookie, Gwenyn::ErrorCode err, Args&&... args)
{
  QVariantList variants = packVariantList(args...);
  call(connID, "nack", cookie, err, variants);
}

void GwenynServer::userPrivMsg(const QString& account, const IRCName& messageSender, const QByteArray& messageTarget, const QByteArray& message)
{
  GwenynUser* user = users[account];
  //qDebug() << messageSender.assemble() << messageTarget << message;
  QString source = QString::fromUtf8(messageTarget + ":" + messageSender.nick);
  if (messageSender.nick == account.toUtf8()) {
    source = ">" + source;
  }
  //call(user->connections, "partyChat", source, QString::fromUtf8(message));
  GwenynProto.partyChat(user->connections, source, QString::fromUtf8(message));
  if(user->connections.isEmpty()) {
    user->queuedMessages << (GwenynUser::QueuedMessage){ source, user->name, QString::fromUtf8(message) };
  }
}

void GwenynServer::userBuddyState(const QString& target, const IRCName& name, const QString& status, const QString& statusMessage)
{
  GwenynUser* user = users[target];
  // TODO: better encapsulation
  call(user->connections, "buddyState", QString::fromUtf8(name.assemble()), status, statusMessage);
}
