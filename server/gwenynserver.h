#ifndef GWENYNSERVER_H
#define GWENYNSERVER_H

#include <QHash>
#include <QSet>
#include "qxtrpcpeer.h"
#include "gwenynuser.h"
#include "gwenynproto.h"

class GwenynServer : public QxtRPCPeer
{
Q_OBJECT
public:
  GwenynServer(QObject* parent = 0);

private slots:
  void newConnection(QIODevice*, qulonglong connID);
  void disconnected(QIODevice*, qulonglong connID);

  void setUsername(qulonglong connID, qulonglong cookie, const QString& name, const QString& password);
  // TODO: presence
  void partyChat(qulonglong connID, qulonglong cookie, const QString& recipient, const QString& message);
  void userPrivMsg(const QString& target, const IRCName& name, const QByteArray& sender, const QByteArray& message);
  void userBuddyState(const QString& target, const IRCName& name, const QString& status, const QString& statusMessage);

  void ack(qulonglong connID, qulonglong cookie);
  void pong(qulonglong cookie);

private:
  template <typename... Args>
  void nack(qulonglong connID, qulonglong cookie, Gwenyn::ErrorCode err, Args&&... args);

  QSet<qulonglong> pendingConnections;
  QHash<qulonglong, GwenynUser*> usersByConnection;
  QHash<QString, GwenynUser*> users;

  Gwenyn gwenynInstance;
};

#endif
