TEMPLATE    = app
DESTDIR     = ..
OBJECTS_DIR = .tmp
MOC_DIR     = .tmp
TARGET      = gwenyn_server
QT          = core network
CONFIG     -= app_bundle
CONFIG     += link_prl
CONFIG     += link_pkgconfig
PKGCONFIG  += openssl

HEADERS += bitlbeeservice.h   gwenynuser.h   gwenynserver.h   keyfiles.h
SOURCES += bitlbeeservice.cpp gwenynuser.cpp gwenynserver.cpp keyfiles.cpp main.cpp

INCLUDEPATH    += ../common ../common/qxtfork
LIBS           += -L.. -lcommon
unix {
    PRE_TARGETDEPS += ../libcommon.a
}
else {
    PRE_TARGETDEPS += ../libcommon.lib
}
