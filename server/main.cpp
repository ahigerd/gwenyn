#include <QCoreApplication>
#include <QStandardPaths>
#include <QStringList>
#include <QSettings>
#include <QDateTime>
#include <QDir>
#include "qxtdaemon.h"
#include "gwenynserver.h"
#include "gwenynproto.h"

int main(int argc, char** argv) {
    QCoreApplication app(argc, argv);
    app.setApplicationName("gwenyn_server");
    app.setOrganizationDomain("alkahest.org");
    app.setOrganizationName("Alkahest");

    QxtDaemon daemon;
    int port;
    /* scope */ {
      QSettings settings;
      if (!settings.contains("common/datadir")) {
        QString datadir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        if (!QDir(datadir).exists()) {
          QDir().mkpath(datadir);
        }
        settings.setValue("common/datadir", datadir);
      }
      QString basePath = settings.value("common/datadir").toString();
      daemon.setLogPath(basePath + "/gwenyn_server.log");
      port = settings.value("common/port", 60668).toInt();
    }

    if(!app.arguments().contains("--no-fork")) {
        bool ok = daemon.daemonize(false);
        if(!ok) {
          qFatal("error in daemonizing");
        }
    }

    if(app.arguments().contains("--alt-port")) {
      port = 60669;
    }

    GwenynServer server;
    if(!server.listen(QHostAddress::Any, port)) {
      qFatal("failed to bind to listening port");
    }
    qDebug() << "Server started. Listening.";

    return app.exec();
}
