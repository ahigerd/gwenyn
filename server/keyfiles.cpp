#include "keyfiles.h"
#include <QtDebug>
#include <QFile>
#include <QSettings>
#include <QCoreApplication>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <cstdio>
#ifdef Q_OS_MAC
#include <unistd.h>
#endif

template <typename T, void (*deleter)(T*)>
class OpenSSLPointer
{
public:
  OpenSSLPointer(T* ptr) : ptr(ptr) {}
  ~OpenSSLPointer() { deleter(ptr); }

  operator T*() const { return ptr; }
  T* operator->() const { return ptr; }

private:
  T* ptr;
};

class FilePointer
{
public:
  FilePointer(const QString& fn)
  : file(std::fopen(fn.toLocal8Bit().constData(), "w+b")), qFile(fn), used(false)
  {
    if(!file) return;
    qFile.open(file, QIODevice::ReadWrite, QFile::AutoCloseHandle);
  }

  ~FilePointer()
  {
    if(!used)
      qFile.remove();
  }

  operator FILE*() const { return file; }

  QFile* device()
  {
    used = true;
    rewind(file);
    return &qFile;
  }

private:
  FILE* file;
  QFile qFile;
  bool used;
};

SslKeyPair generateCertificates(const QString& basePath)
{
  // First, generate the private key
  OpenSSLPointer<EVP_PKEY, EVP_PKEY_free> key = EVP_PKEY_new();
  if(!key)
    return SslKeyPair();
  // The RSA object doesn't need to be freed because the EVP_PKEY object takes ownership of it.
  RSA* rsa = RSA_generate_key(2048, 65537, 0, 0);
  if(!EVP_PKEY_assign_RSA(key, rsa))
    return SslKeyPair();

  // Then generate the certificate
  OpenSSLPointer<X509, X509_free> x509 = X509_new();
  if(!x509)
    return SslKeyPair();
  ASN1_INTEGER_set(X509_get_serialNumber(x509), 1);
  X509_gmtime_adj(X509_get_notBefore(x509), 0);
  X509_gmtime_adj(X509_get_notAfter(x509), 315360000L); // 10 years
  X509_set_pubkey(x509, key);

  // Then sign the certificate
  X509_NAME* name = X509_get_subject_name(x509);
  X509_NAME_add_entry_by_txt(name, "O",  MBSTRING_ASC, (unsigned char *)"Gwenyn Server", -1, -1, 0);
  X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, (unsigned char *)"gwenyn", -1, -1, 0);
  X509_set_issuer_name(x509, name);
  if(!X509_sign(x509, key, EVP_sha1()))
    return SslKeyPair();

  // Write the private key to a file
  FilePointer keyFile(QCoreApplication::applicationDirPath() + "/gwenyn_private.pem");
  if(!keyFile || !PEM_write_PrivateKey(keyFile, key, 0, 0, 0, 0, 0))
    return SslKeyPair();

  // Write the public key to a file
  FilePointer certFile(QCoreApplication::applicationDirPath() + "/gwenyn_public.pem");
  if(!certFile || !PEM_write_X509(certFile, x509))
    return SslKeyPair();

  // Update QSettings
  QSettings settings;
  settings.setValue("ssl/certificate", basePath + "/gwenyn_public.pem");
  settings.setValue("ssl/privatekey", basePath + "/gwenyn_private.pem");

  // Convert the keys to Qt objects and return them
  return SslKeyPair(QSslCertificate(certFile.device()), QSslKey(keyFile.device(), QSsl::Rsa));
}

QPair<QSslCertificate, QSslKey> getCertificates(bool generateIfMissing)
{
  QSettings settings;
  QString basePath = settings.value("general/datadir", QCoreApplication::applicationDirPath()).toString();
  QFile certFile(settings.value("ssl/certificate", basePath + "/gwenyn_public.pem").toString());
  QFile keyFile(settings.value("ssl/privatekey", basePath + "/gwenyn_private.pem").toString());
#ifdef Q_OS_MAC
  // Use access() instead of QFile::exists() on Mac due to the latter causing a crash when daemonized
  if(!::access(certFile.fileName().toLocal8Bit().constData(), F_OK) != -1 &&
     !::access(keyFile.fileName().toLocal8Bit().constData(), F_OK) != -1) {
#else
  if(!certFile.exists() && !keyFile.exists()) {
#endif
    if(generateIfMissing) {
      return generateCertificates(basePath);
    }
    return SslKeyPair();
  }
  certFile.open(QIODevice::ReadOnly);
  keyFile.open(QIODevice::ReadOnly);
  return SslKeyPair(QSslCertificate(&certFile), QSslKey(&keyFile, QSsl::Rsa));
}
