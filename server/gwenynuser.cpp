#include "gwenynuser.h"

GwenynUser::GwenynUser(const QString& name)
: name(name), bitlbee(0), useBouncer(true)
{
  // initializers only
}

void GwenynUser::connect()
{
  //bitlbee.connect("localhost", 60666);
  // TODO: specify path to bitlbee binary
  if(!bitlbee.launch()) {
    qDebug() << "Failed to launch bitlbee";
  }
}
