#ifndef KEYFILES_H
#define KEYFILES_H

#include <QSslCertificate>
#include <QSslKey>
#include <QPair>

typedef QPair<QSslCertificate, QSslKey> SslKeyPair;

SslKeyPair generateCertificates(const QString& basePath);
SslKeyPair getCertificates(bool generateIfMissing = true);

#endif
