#ifndef BITLBEESERVICE_H
#define BITLBEESERVICE_H

#include <QObject>
#include <QHostAddress>
#include <QPointer>
#include <QQueue>
#include <QTimer>
#include "ircserializer.h"
#include "variantlist.h"
#include "gwenynproto.h"
class QAbstractSocket;
class QProcess;
class QxtRPCService;

class BitlbeeService : public QObject
{
Q_OBJECT
public:
    BitlbeeService(QObject* parent = 0);
    ~BitlbeeService();

    // TODO: ugly. ident provider?
    void setIdent(const QString& username, const QString& password);
    bool launch();
    bool isConnected() const;

public slots:
    void disconnect();
    void requestBuddyList();

    void sendMessage(qulonglong cookie, const QString& account, const QString& recipient, const QString& message);

signals:
    void receivedMessage(const IRCName& source, const QByteArray& dest, const QByteArray& message);
    void buddyState(const IRCName& buddy, const QString& status, const QString& statusMessage);

private slots:
    void crashed();
    void pingResponse(const IRCName& /* unused */, const QByteArray& token);
    void privmsg(const IRCName& source, const QByteArray& dest, const QByteArray& message);
    void buddyJoin(const IRCName& buddy, const QByteArray& channel);
    void buddyPart(const IRCName& buddy, const QByteArray& channel);
    void buddyQuit(const IRCName& buddy);
    void buddyMode(const IRCName& source, const QByteArray& channel, const QByteArray& modes, const QByteArray& modeParam1 = QByteArray());
    void sendStatusUpdates();

    void workerBuddyList(qulonglong cookie, const QString& account, Gwenyn::StringMap list);
    void workerBuddyStatus(qulonglong cookie, const QString& account, const QString& buddy, const QString& status);
    void workerBuddyMessage(qulonglong cookie, const QString& account, const QString& buddy, const QString& message);

private:
    void setService(QxtRPCService* newService);

    QProcess* bitlbeeProcess;
    QPointer<QxtRPCService> service;
    QString m_id;
    QString m_password;

    enum ConnectionState {
        Stopped,
        NeedsIdentify,
        NeedsIdentifyConfirmation,
        Running
    };
    ConnectionState state;

    enum MetadataState {
      Idle,
      ReadingBList,
      ReadingChanList
    };
    QQueue<MetadataState> metadataState;

    void updateStatus(const IRCName& buddy, const QString& status, const QString& statusMessage = QString());
    QMap<IRCName, QString> pendingStatus;
    QMap<IRCName, QString> pendingStatusMessage;
    bool pendingBList;
    QTimer pendingStatusTimer;
};

#endif
