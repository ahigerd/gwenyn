#ifndef GWENYNUSER_H
#define GWENYNUSER_H

#include "gwenynproto.h"
#include "bitlbeeservice.h"
#include <QList>
#include <QDateTime>

class GwenynUser
{
public:
  GwenynUser(const QString& name);

  struct QueuedMessage {
    QString sender;
    QString recipient;
    QString message;
    QDateTime timestamp;
  };

  QString name;
  BitlbeeService bitlbee;
  QList<qulonglong> connections;
  bool useBouncer;
  QList<QueuedMessage> queuedMessages;

  void connect();
};

#endif
