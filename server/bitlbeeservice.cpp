#include "bitlbeeservice.h"
#include <QCoreApplication>
#include <QSettings>
#include <QStringList>
#include <QProcess>
#include <QTcpSocket>
#include "qxtrpcpeer.h"
#include <unistd.h>
#include <sys/socket.h>
#include <signal.h>
#ifndef Q_OS_DARWIN
#include <sys/wait.h>
#endif

BitlbeeService::BitlbeeService(QObject* parent)
: QObject(parent), bitlbeeProcess(0), service(0), state(Stopped), pendingBList(false)
{
  m_id = "gwenyn_" + QString::number(quintptr(this));

  pendingStatusTimer.setInterval(1000);
  pendingStatusTimer.setSingleShot(true);
  QObject::connect(&pendingStatusTimer, SIGNAL(timeout()), this, SLOT(sendStatusUpdates()));
}

BitlbeeService::~BitlbeeService()
{
  disconnect();
}

bool BitlbeeService::launch()
{
  if(bitlbeeProcess) return false;

  QSettings settings;

  bitlbeeProcess = new QProcess(this);
  bitlbeeProcess->setProcessChannelMode(QProcess::ForwardedErrorChannel);
  bitlbeeProcess->setReadChannel(QProcess::StandardOutput);
  bitlbeeProcess->start(QString("%1/gwenyn_worker --userdir=%2/%3")
      .arg(QCoreApplication::applicationDirPath())
      .arg(settings.value("common/datadir").toString())
      .arg(m_id));
  setService(new QxtRPCService(bitlbeeProcess, this));
  //QObject::connect(bitlbeeProcess, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(crashed()));
  QObject::connect(bitlbeeProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(crashed()));
  bool ok = bitlbeeProcess->waitForStarted();
  qDebug() << "process" << ok;
  return ok;
}

void BitlbeeService::crashed()
{
  qFatal("Child bitlbee process crashed, aborting!");
  qDebug() << "Child bitlbee process crashed, relaunching...";
  disconnect();
  launch();
}

bool BitlbeeService::isConnected() const
{
  return service != 0;
}

void BitlbeeService::setService(QxtRPCService* newService)
{
  // TODO: asynchronous connect, connection retry, signals
  if(service) {
    service->deleteLater();
  }
  service = newService;
  service->attachSlot("accountBuddyList", this, SLOT(workerBuddyList(qulonglong, QString, Gwenyn::StringMap)));
  service->attachSlot("buddyStatus", this, SLOT(workerBuddyStatus(qulonglong, QString, QString, QString)));
  service->attachSlot("buddyMessage", this, SLOT(workerBuddyMessage(qulonglong, QString, QString, QString)));
  state = Running;
}

void BitlbeeService::pingResponse(const IRCName& /* unused */, const QByteArray& token)
{
  //service->call("PONG", "", token);
}

void BitlbeeService::requestBuddyList()
{
  pendingBList = true;
  pendingStatusTimer.start();
}

void BitlbeeService::privmsg(const IRCName& source, const QByteArray& dest, const QByteArray& message)
{
  if(dest[0] == '&' && message.endsWith("Logged in")) {
    requestBuddyList();
  }
  if(dest == "&__gwenyn__") {
    MetadataState mstate = metadataState.isEmpty() ? Idle : metadataState.head();
    if(mstate == ReadingBList) {
      if(message.contains(" buddies (")) {
        metadataState.dequeue();
        return;
      }
      QString umessage = QString::fromUtf8(message);
      if(umessage.contains("Handle/Account")) return;
      QString statusMessage = umessage.section('(', 1).mid(1);
      statusMessage.chop(2);
      umessage = umessage.section('(', 0, 0).simplified();
      umessage.replace("Do Not Disturb", "DND");
      QStringList fields = umessage.section('(', 0, 0).simplified().split(' ');
      QByteArray status = fields.takeLast().toUtf8();
      if(fields[1].endsWith("@public.talk.g")) {
        fields[1] += "oogle.com";
        fields.insert(2, "gtalk"); // TODO: what if there's >1 gtalk account?
      }
      QByteArray nick = fields.takeFirst().toUtf8();
      QByteArray host = fields.takeLast().toUtf8();
      QByteArray ident = fields.join(' ').toUtf8();
      if(ident.contains('@')) {
        auto parts = ident.split('@');
        ident = parts[0];
        host = parts[1];
      }
      updateStatus(IRCName(nick, ident, host), status, statusMessage);
    } else if(mstate == ReadingChanList) {
      if(message.contains("chat channel")) {
        QString channelName = QString::fromUtf8(message).section(' ', 1, 1).replace(",", "");

        if(!message.contains("(joined)")) {
          //service->call("JOIN", "", channelName.toUtf8());
        }

        // TODO: this might not work for all channels
        emit buddyState(IRCName(channelName.toUtf8()), "Online", QString());
      }
    } else {
      // For now, pass on unrecognized/unsolicited messages to the control channel
      emit receivedMessage(IRCName("gwenyn"), "&bitlbee", message);
    }
    return;
  }
  emit receivedMessage(source, dest, message);
  if(dest.startsWith('&')) {
    if(state == NeedsIdentify) {
      //service->call("PRIVMSG", "", "&bitlbee", "identify " + m_password);
      state = NeedsIdentifyConfirmation;
    } else if(state == NeedsIdentifyConfirmation && message.contains("Password accepted, settings and accounts loaded")) {
      state = Running;
      //service->call("JOIN", "", "&__gwenyn__");
    }
  }
}

void BitlbeeService::updateStatus(const IRCName& buddy, const QString& status, const QString& statusMessage)
{
  if(!status.isEmpty()) {
    pendingStatus[buddy] = status;
  }
  if(!statusMessage.isNull()) {
    pendingStatusMessage[buddy] = statusMessage;
  }
  pendingStatusTimer.start();
}

void BitlbeeService::buddyJoin(const IRCName& buddy, const QByteArray& channel)
{
  if(channel == "&bitlbee") {
    updateStatus(buddy, "Away");
  }
}

void BitlbeeService::buddyPart(const IRCName& buddy, const QByteArray& channel)
{
  if(channel == "&bitlbee") {
    updateStatus(buddy, "Offline");
  }
}

void BitlbeeService::buddyQuit(const IRCName& buddy)
{
  updateStatus(buddy, "Offline");
}

void BitlbeeService::buddyMode(const IRCName& source, const QByteArray& channel, const QByteArray& modes, const QByteArray& modeParam1)
{
  if(modes == "+v") {
    updateStatus(IRCName(modeParam1), "Online");
  } else if(modes == "-v") {
    updateStatus(IRCName(modeParam1), "Away");
  }
}

void BitlbeeService::sendStatusUpdates()
{
  if(pendingBList) {
    pendingBList = false;
    //service->call("PRIVMSG", "", "&__gwenyn__", "blist all");
    metadataState.enqueue(ReadingBList);
    pendingStatusTimer.start();
    return;
  }
  for(const IRCName& buddy : pendingStatus.keys()) {
    emit buddyState(buddy, pendingStatus[buddy], pendingStatusMessage.value(buddy));
    pendingStatusMessage.remove(buddy);
  }
  for(const IRCName& buddy : pendingStatusMessage.keys()) {
    emit buddyState(buddy, QString(), pendingStatusMessage[buddy]);
  }
  pendingStatus.clear();
  pendingStatusMessage.clear();
}

void BitlbeeService::setIdent(const QString& username, const QString& password)
{
  m_id = username;
  m_password = password;
}

void BitlbeeService::disconnect()
{
  if(bitlbeeProcess) {
    bitlbeeProcess->deleteLater();
    bitlbeeProcess = 0;
  }
  if(service) {
    service->deleteLater();
    service = 0;
  }
}

void BitlbeeService::workerBuddyList(qulonglong cookie, const QString& account, Gwenyn::StringMap list)
{
  service->call("accountBuddyList_ack", cookie);
  for(const QString& buddy : list.keys()) {
    updateStatus(IRCName(buddy.toUtf8(), buddy.toUtf8(), account.toUtf8()), list[buddy]);
  }
}

void BitlbeeService::workerBuddyStatus(qulonglong cookie, const QString& account, const QString& buddy, const QString& status)
{
  service->call("buddyStatus_ack", cookie);
  updateStatus(IRCName(buddy.toUtf8(), buddy.toUtf8(), account.toUtf8()), status);
}

void BitlbeeService::workerBuddyMessage(qulonglong cookie, const QString& account, const QString& buddy, const QString& message)
{
  service->call("buddyMessage_ack", cookie);
  emit receivedMessage(IRCName(buddy.toUtf8(), buddy.toUtf8(), account.toUtf8()), buddy.toUtf8(), message.toUtf8());
}

void BitlbeeService::sendMessage(qulonglong cookie, const QString& account, const QString& recipient, const QString& message)
{
  service->call("buddyMessage", cookie, account, recipient, message);
  GwenynProto.partyChat.ack(cookie);
}
