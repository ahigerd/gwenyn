TEMPLATE    = app
DESTDIR     = ..
OBJECTS_DIR = .tmp
MOC_DIR     = .tmp
TARGET      = gwenyn_worker
QT          = core network
CONFIG     -= app_bundle
CONFIG     += link_prl
CONFIG     += link_pkgconfig
PKGCONFIG  += purple glib-2.0

HEADERS += gwenynworker.h
SOURCES += gwenynworker.cpp worker.cpp

INCLUDEPATH    += ../common ../common/qxtfork ../libpurple-qt
LIBS           += -L.. -lcommon -lpurple-qt
unix {
    PRE_TARGETDEPS += ../libpurple-qt.a ../libcommon.a
}
else {
    PRE_TARGETDEPS += ../libpurple-qt.lib ../libcommon.lib
}

