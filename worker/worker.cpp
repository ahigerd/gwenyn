#include <QCoreApplication>
#include <signal.h>
#include "qxtcommandoptions.h"
#include "pqcore.h"
#include "pqaccount.h"
#include "pqconversation.h"
#include "pqbuddy.h"
#include "gwenynworker.h"
#include "gwenynproto.h"
#include <QtDebug>
#include <QTimer>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char** argv)
{
  QCoreApplication app(argc, argv);

  QxtCommandOptions opt;
  // TODO: more options are always good ;)
  opt.add("userdir", "The path to the Purple user data directory", QxtCommandOptions::ValueRequired);
  opt.parse(app.arguments());

  PQCore core("gwenyn");
  GwenynWorker worker;

  if(opt.count("userdir")) {
    PQCore::setUserDir(opt.value("userdir").toString());
  }

  core.setPreference("/purple/logging/log_ims", false);
  core.setPreference("/purple/logging/log_chats", false);
  core.start();
  //worker.initializeAccounts();


  /*
  purple_pounces_load();
  */

  QTimer reaper;
  QObject::connect(&reaper, &QTimer::timeout, []{ waitpid(-1, 0, WNOHANG); });
  reaper.start(15000);


  return app.exec();
}
