#ifndef GWENYNWORKER_H
#define GWENYNWORKER_H

#include "gwenynproto.h"
#include "pqaccount.h"
#include "pqconversation.h"

class GwenynWorker : public Gwenyn
{
Q_OBJECT
public:
  GwenynWorker(QIODevice* device = nullptr);

private slots:
  void initializeAccounts();
  void connectionProgress(const QString& text, int step, int count);
  void connected();
  void reportError(PQAccount::ConnectionError errorType, const QString& error);

  void newBuddy(PQBuddy* buddy);
  void newConversation(PQConversation* conv);
  void receivedMessage(const QString& from, const QString& message, PQConversation::MessageFlags flags);
  void sendMessage(qulonglong cookie, const QString& account, const QString& to, const QString& message);
};

#endif
