#include "gwenynworker.h"
#include "qxtrpcservice.h"
#include "qxtstdio.h"
#include "pqcore.h"
#include "pqbuddy.h"
#include "pqconversation.h"
// TODO: make PQDebug or somethign
#include <libpurple/debug.h>
#include <QTimer>

GwenynWorker::GwenynWorker(QIODevice* device)
: Gwenyn(new QxtRPCService(device ? device : new QxtStdio()), false)
{
  m_rpc->setParent(this);
  QObject::connect(PQCore::instance(), SIGNAL(initialized()), this, SLOT(initializeAccounts()));

  using namespace std::placeholders;
  buddyMessage.bind(std::bind(&GwenynWorker::sendMessage, this, _1, _2, _3, _4));
}

void GwenynWorker::initializeAccounts()
{
  qDebug() << "**** initializeAccounts";
  for(PQAccount* acct : PQAccount::allAccounts()) {
    QObject::connect(
        acct, SIGNAL(connectionProgress(QString, int, int)),
        this, SLOT(connectionProgress(QString, int, int)));
    QObject::connect(
        acct, SIGNAL(connected()),
        this, SLOT(connected()));
    QObject::connect(
        acct, SIGNAL(connectionError(PQAccount::ConnectionError, QString)),
        this, SLOT(reportError(PQAccount::ConnectionError, QString)));
    QObject::connect(
        acct, SIGNAL(disconnected(PQAccount::ConnectionError, QString)),
        this, SLOT(reportError(PQAccount::ConnectionError, QString)));
    QObject::connect(
        acct, SIGNAL(newConversation(PQConversation*)),
        this, SLOT(newConversation(PQConversation*)));
    QObject::connect(
        acct, SIGNAL(newBuddy(PQBuddy*)),
        this, SLOT(newBuddy(PQBuddy*)));
    acct->connect();
    QObject::connect(acct, &PQAccount::signedIn, [=]() {
      qDebug() << acct->username() << "connected";
      for(auto buddy : acct->buddies()) {
        qDebug() << buddy->name() << buddy->alias() << buddy->statusName();
      }
    });
  }
}

void GwenynWorker::connected()
{
  qDebug() << "**** connected";
  PQAccount* account = static_cast<PQAccount*>(sender());
  // TODO: account IDs
  accountConnected(account->username());
  QTimer::singleShot(1000, [this, account]{
    Gwenyn::StringMap status;
    for(PQBuddy* buddy : account->buddies()) {
      status[buddy->name()] = buddy->statusName();
    }
    accountBuddyList(account->username(), status);
  });
}

void GwenynWorker::reportError(PQAccount::ConnectionError errorType, const QString& error)
{
  qDebug() << (int)errorType << error;
  PQAccount* account = static_cast<PQAccount*>(sender());
  if(account->connectionState() == PQAccount::Disconnected) {
    // TODO: account IDs
    accountDisconnected(account->username(), error);
  }
}

void GwenynWorker::connectionProgress(const QString& text, int step, int count)
{
  qDebug() << "Connection progress:" << step << "/" << count << ("(" + text + ")");
}

void GwenynWorker::newBuddy(PQBuddy* buddy)
{
  // TODO: do more with this
  if(buddy->account()->connectionState() == PQAccount::Connected) {
    GwenynProto.buddyStatus(buddy->account()->username(), buddy->name(), buddy->statusName());
  }
}

void GwenynWorker::newConversation(PQConversation* conv)
{
  qDebug() << "new conversation with" << conv->name();
  QObject::connect(
      conv, SIGNAL(receivedMessage(QString, QString, PQConversation::MessageFlags)),
      this, SLOT(receivedMessage(QString, QString, PQConversation::MessageFlags)));
}

void GwenynWorker::receivedMessage(const QString& from, const QString& message, PQConversation::MessageFlags flags)
{
  PQConversation* conv = qobject_cast<PQConversation*>(sender());
  // TODO: use account identifier instead of just username
  GwenynProto.buddyMessage(conv->account()->username(), from, message);
}

void GwenynWorker::sendMessage(qulonglong cookie, const QString& account, const QString& to, const QString& message)
{
  // TODO: account identifier
  qDebug() << "sendMessage" << account << to << message;
  PQAccount* pacct = nullptr;
  if(account.isEmpty()) {
    for(PQBuddy* buddy : PQBuddy::allBuddies()) {
      if(buddy->name() == to || buddy->alias() == to) {
        pacct = buddy->account();
        break;
      }
    }
    if(!pacct) {
      purple_debug_error("libpurple-qt", "no buddy matching '%s'", to.toUtf8().constData());
      // TODO: this is the wrong error
      buddyMessage.error(cookie, Gwenyn::NoSuchAccount, account);
      return;
    }
  } else {
    QList<PQAccount*> paccts = PQAccount::findByUsername(account);
    if(paccts.isEmpty()) {
      purple_debug_error("libpurple-qt", "no account with username '%s'", account.toUtf8().constData());
      buddyMessage.error(cookie, Gwenyn::NoSuchAccount, account);
      return;
    }
    pacct = paccts[0];
  }
  buddyMessage.ack(cookie);
  for(PQConversation* conv : pacct->openConversations()) {
    if(conv->name() == to) {
      conv->sendMessage(message);
      return;
    }
  }
  PQConversation* conv = new PQConversation(pacct, to, PQConversation::Direct);
  conv->sendMessage(message);
}

